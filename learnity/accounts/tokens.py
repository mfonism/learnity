from django.contrib.auth.tokens import PasswordResetTokenGenerator


class AccountActivationTokenGenerator(PasswordResetTokenGenerator):
    def _make_hash_value(self, user, timestamp):
        activation_timestamp = (
            "" if user.activated_at is None else user.activated_at.replace(microsecond=0, tzinfo=None)
        )
        return f'{user.pk}{user.is_active}{activation_timestamp}{timestamp}'


account_activation_token_generator = AccountActivationTokenGenerator()
