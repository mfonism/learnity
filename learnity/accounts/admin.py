from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.translation import gettext_lazy as _

from .forms import UserChangeForm, AdminUserCreationForm as UserCreationForm

UserModel = get_user_model()


@admin.register(UserModel)
class UserAdmin(BaseUserAdmin):
    fieldsets = (
        (None, {'fields': ('name', 'email', 'password')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),}),
        (_('Important dates'), {'fields': ('agreed_to_tnc_at', 'activated_at', 'last_login')},),
    )
    add_fieldsets = ((None, {'classes': ('wide',), 'fields': ('name', 'email', 'password1', 'password2'),}),)
    form = UserChangeForm
    add_form = UserCreationForm
    list_display = ('name', 'email', 'is_active', 'is_staff', 'created_at')
    search_fields = ('name', 'email')
    ordering = ('name', '-created_at')
