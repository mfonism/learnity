from django.urls import path

from .views import (
    ActivationView,
    ActivationDoneView,
    ActivationConfirmView,
    PasswordChangeView,
    PasswordChangeDoneView,
    PasswordResetView,
    PasswordResetCompleteView,
    PasswordResetConfirmView,
    PasswordResetDoneView,
    SignupDoneView,
)

app_name = 'accounts'

# signup endpoints
urlpatterns = [path('signup/done/', SignupDoneView.as_view(), name='signup_done')]

# account activation endpoints
urlpatterns += [
    path('activation/', ActivationView.as_view(), name='activation'),
    path('activation/done/', ActivationDoneView.as_view(), name='activation_done'),
    path('activation/<uidb64>/<token>/', ActivationConfirmView.as_view(), name='activation_confirm'),
]

# password change endpoints
urlpatterns += [
    path('password_change/', PasswordChangeView.as_view(), name='password_change'),
    path('password_change/done/', PasswordChangeDoneView.as_view(), name='password_change_done'),
]

# password reset endpoints
urlpatterns += [
    path('password_reset/', PasswordResetView.as_view(), name='password_reset'),
    path('password_reset/done/', PasswordResetDoneView.as_view(), name='password_reset_done'),
    path('reset/<uidb64>/<token>/', PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    path('reset/done/', PasswordResetCompleteView.as_view(), name='password_reset_complete'),
]
