{% load i18n %}{% autoescape off %}
{% translate 'Hey there' %}, {{ user.name }}!

{% blocktranslate with site_name=site.name %}Please use the following link to activate your {{ site_name }} account:{% endblocktranslate %}

https://{{ site.domain }}{% url 'accounts:activation_confirm' uidb64=uidb64 token=token %}
{% endautoescape %}
