from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from accounts import services

UserModel = get_user_model()


class TestActivationEmailServices(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = UserModel.objects.create_user(
            name='Nenn Mich User', email='user@aol.com', password='Dies ist eine Passphrase'
        )

    @mock.patch('accounts.services.get_activation_email_subject', return_value='mock subject')
    @mock.patch('accounts.services.get_activation_email_body', return_value='mock body')
    @mock.patch('accounts.services.EmailMessage')
    def test_send_activation_email(self, mock_email_message, mock_get_body, mock_get_subject):
        services.send_activation_email(self.user)

        mock_get_subject.assert_called_once()
        mock_get_body.assert_called_once_with(self.user)
        mock_email_message.assert_called_once_with(
            subject=mock_get_subject.return_value, body=mock_get_body.return_value, to=[self.user]
        )
        mock_email_message.return_value.send.assert_called_once()

    def test_get_activation_email_subject(self):
        subject = services.get_activation_email_subject()
        expected_subject = f'Please activate your {settings.CUSTOM_SITE_NAME} account!'

        self.assertEqual(subject, expected_subject)

    @mock.patch('accounts.tokens.account_activation_token_generator.make_token', return_value='diesIstEinScheinzeichen')
    def test_get_activation_email_body(self, mock_make_token):
        message = services.get_activation_email_body(self.user)

        mock_make_token.assert_called_once_with(self.user)
        self.assertIn(f'Hey there, {self.user.name}!', message)
        uidb64 = urlsafe_base64_encode(force_bytes(self.user.uuid))
        full_url = settings.CUSTOM_SITE_DOMAIN + reverse(
            'accounts:activation_confirm', kwargs={'uidb64': uidb64, 'token': mock_make_token.return_value}
        )
        self.assertIn(full_url, message)
