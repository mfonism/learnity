from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from accounts.tests.mixins import UserTestCaseMixin


class TestPasswordChangeView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('accounts:password_change')
        cls.success_alert = _('You\'ve just changed your password.')

    def setUp(self):
        super().setUp()
        # one of the tests changes the password
        # so we need to reset it for every test
        self.user.refresh_from_db()

    def test_get_page(self):
        self.client.force_login(self.user)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_change.html')
        self.assertContains(resp, 'id_old_password')
        self.assertContains(resp, 'id_new_password1')
        self.assertContains(resp, 'id_new_password2')
        self.assertContains(resp, 'Change password')
        # test that a few extra things on the built-in Django form don't show up here
        self.assertNotContains(resp, _('New password confirmation'))
        self.assertContains(resp, _('Confirm new password'))
        self.assertNotContains(resp, _('Your password can’t be too similar to your other personal information.'))
        self.assertNotContains(resp, _('Your password must contain at least 8 characters.'))
        self.assertNotContains(resp, _('Your password can’t be a commonly used password.'))
        self.assertNotContains(resp, _('Your password can’t be entirely numeric.'))

    def test_change_password(self):
        newpassword = 'Dies ist eine neue Passphrase'
        payload = {'old_password': self.password, 'new_password1': newpassword, 'new_password2': newpassword}
        self.client.force_login(self.user)

        resp = self.client.post(self.url, payload)

        self.assertRedirects(resp, reverse('accounts:password_change_done'))
        self.user.refresh_from_db()
        self.assertEqual(self.user.check_password(self.password), False)
        self.assertEqual(self.user.check_password(newpassword), True)
        self.assertEqual(resp.wsgi_request.user.is_authenticated, True)

        message_storage = get_messages(resp.wsgi_request)
        messages = [str(message) for message in message_storage if 'password_change' in message.tags.split()]
        self.assertIn(self.success_alert, messages)
