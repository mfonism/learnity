from unittest import mock

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from accounts.tests.mixins import UserTestCaseMixin


class TestActivationView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        cls.setUpUser()
        cls.url = reverse('accounts:activation')

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/activation.html')
        self.assertContains(resp, 'id_email')
        self.assertContains(resp, 'id_password')
        self.assertContains(resp, 'Send me an activation link')

    @mock.patch('accounts.services.send_activation_email')
    def test_request_activation_link(self, mock_send_email):
        resp = self.client.post(self.url, {'email': self.email, 'password': self.password})

        self.assertRedirects(resp, reverse('accounts:activation_done'))
        mock_send_email.assert_called_once_with(self.user)

    @mock.patch('accounts.services.send_activation_email')
    def test_activation_link_request_fails_for_unknown_email(self, mock_send_email):
        resp = self.client.post(self.url, {'email': 'unknown@aol.com', 'password': self.password})

        self.assertEqual(resp.status_code, 200)
        expected_error_message = _(
            'Please enter a correct email address and password. Note that both fields may be case-sensitive.'
        )
        self.assertContains(resp, expected_error_message)
        mock_send_email.assert_not_called()

    @mock.patch('accounts.services.send_activation_email')
    def test_activation_link_request_fails_for_wrong_password(self, mock_send_email):
        resp = self.client.post(self.url, {'email': self.email, 'password': 'Kein Passwort'})

        self.assertEqual(resp.status_code, 200)
        expected_error_message = _(
            'Please enter a correct email address and password. Note that both fields may be case-sensitive.'
        )
        self.assertContains(resp, expected_error_message)
        mock_send_email.assert_not_called()

    @mock.patch('accounts.services.send_activation_email')
    def test_activation_link_requested_for_already_activated_user(self, mock_send_email):
        self.user.activated_at = timezone.now()
        self.user.save()

        resp = self.client.post(self.url, {'email': self.email, 'password': self.password})

        self.assertRedirects(resp, reverse('accounts:activation_done'))
        mock_send_email.assert_not_called()
