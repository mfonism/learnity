from django.conf import settings
from django.test import TestCase
from django.urls import reverse


class TestSignupDoneView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('accounts:signup_done')

    def test_get_signup_done_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/signup_done.html')
        self.assertContains(resp, settings.CUSTOM_SITE_NAME)
        self.assertContains(resp, 'request a new activation link')
        self.assertContains(resp, reverse('accounts:activation'))
