from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from accounts.tests.mixins import UserTestCaseMixin


class TestPasswordChangeDoneView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('accounts:password_change_done')
        cls.success_alert = _('You\'ve just changed your password.')

    def setUp(self):
        super().setUp()
        self.user.refresh_from_db()

    def test_get_page(self):
        self.client.force_login(self.user)
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_change_done.html')
        self.assertTemplateUsed(resp, 'accounts/password_change_done.html')

    def test_page_content_from_password_change(self):
        payload = {
            'old_password': self.password,
            'new_password1': 'Dies ist eine neue Passphrase',
            'new_password2': 'Dies ist eine neue Passphrase',
        }
        self.client.force_login(self.user)

        resp = self.client.post(reverse('accounts:password_change'), payload, follow=True)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_change_done.html')
        # this page was redirected to from a password change action
        # so there should be an appropriate message in it
        self.assertContains(resp, self.success_alert)
