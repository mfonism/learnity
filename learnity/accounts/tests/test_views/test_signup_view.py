from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

UserModel = get_user_model()


class TestSignupView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('signup')
        cls.email = 'user@aol.com'
        cls.name = 'Nenn Mich User'
        cls.passphrase = 'Dies ist eine Passphrase'
        cls.payload = {
            'name': cls.name,
            'email': cls.email,
            'password1': cls.passphrase,
            'password2': cls.passphrase,
            'agree_to_tnc': True,
        }

    def test_get_signup_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/signup.html')
        self.assertContains(resp, 'id_name')
        self.assertContains(resp, 'id_email')
        self.assertContains(resp, 'id_password1')
        self.assertContains(resp, 'id_password2')
        self.assertContains(resp, 'id_agree_to_tnc')
        self.assertContains(resp, 'Sign up')

    @mock.patch('accounts.services.send_activation_email')
    def test_signup(self, mock_send_email):
        old_user_count = UserModel.objects.count()

        resp = self.client.post(self.url, self.payload)

        self.assertRedirects(resp, reverse('accounts:signup_done'))
        user_count = UserModel.objects.count()
        self.assertEqual(user_count, old_user_count + 1)
        created_user = UserModel.objects.latest('created_at')
        self.assertEqual(created_user.name, self.name)
        self.assertEqual(created_user.email, self.email)
        self.assertEqual(created_user.check_password(self.passphrase), True)
        self.assertIsNotNone(created_user.agreed_to_tnc_at)
        self.assertEqual(created_user.is_active, False)
        self.assertIsNone(created_user.activated_at)
        mock_send_email.assert_called_once_with(created_user)

    def test_signup_fails_if_not_agree_to_tnc(self):
        old_user_count = UserModel.objects.count()

        resp = self.client.post(self.url, {**self.payload, 'agree_to_tnc': False})

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/signup.html')
        user_count = UserModel.objects.count()
        self.assertEqual(user_count, old_user_count)
        errors = resp.context['form'].errors
        self.assertIn(_('It is required that you agree to the terms of use.'), errors['agree_to_tnc'])
