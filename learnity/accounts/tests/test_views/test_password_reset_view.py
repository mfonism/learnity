from unittest import mock

from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse

from accounts.tests.mixins import UserTestCaseMixin


class TestPasswordResetView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.payload = {'email': cls.email}
        cls.success_alert = 'We\'ve sent you an email containing instructions for resetting your password.'
        cls.url = reverse('accounts:password_reset')

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_reset.html')
        self.assertContains(resp, 'id_email')
        self.assertContains(resp, 'Send me password reset instructions')

    @mock.patch('accounts.views.PasswordResetForm.send_mail')
    def test_password_reset_link_is_sent_on_posting_correct_email(self, mock_send_mail):
        resp = self.client.post(self.url, self.payload)

        self.assertRedirects(resp, reverse('accounts:password_reset_done'))
        # assert that correct success alert is added to message storage
        message_storage = get_messages(resp.wsgi_request)
        messages = [str(message) for message in message_storage if 'password_reset' in message.tags.split()]
        self.assertIn(self.success_alert, messages)
        # assert that email sending function is called with the correct arguments
        mock_send_mail.assert_called_once()
        args = mock_send_mail.call_args.args
        subject_template_name, email_template_name, context, _from_email, to_email, *rest = args
        self.assertEqual(subject_template_name, 'accounts/email/password_reset_subject.txt')
        self.assertEqual(email_template_name, 'accounts/email/password_reset_email.txt')
        self.assertEqual(context['email'], self.email)
        self.assertEqual(context['user'], self.user)
        self.assertEqual(to_email, self.email)
