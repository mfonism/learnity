from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from accounts.tests.mixins import UserTestCaseMixin


class TestPasswordResetConfirmView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.new_password = 'Dies ist ein neues Passwort'
        cls.success_alert = 'You\'ve successfully reset your password. You may now go ahead and log in.'
        cls.url = reverse(
            'accounts:password_reset_confirm', kwargs={'uidb64': cls.get_url_safe_uuid(), 'token': cls.make_token()}
        )

    def test_get_page(self):
        resp = self.client.get(self.url, follow=True)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/password_reset_confirm.html')
        self.assertContains(resp, 'id_new_password1')
        self.assertContains(resp, 'id_new_password2')
        self.assertContains(resp, 'Reset my password')

        # check that the default help texts do not
        # show up for the first password field
        self.assertNotContains(resp, _('Your password can’t be too similar to your other personal information.'))
        self.assertNotContains(resp, _('Your password must contain at least 8 characters.'))
        self.assertNotContains(resp, _('Your password can’t be a commonly used password.'))
        self.assertNotContains(resp, _('Your password can’t be entirely numeric.'))

        # check that the label of the password confirmation
        # is different from the default
        self.assertNotContains(resp, _('New password confirmation'))
        self.assertContains(resp, _('Confirm new password'))

    def test_reset_password(self):
        _resp = self.client.get(self.url, follow=True)
        payload = {'new_password1': self.new_password, 'new_password2': self.new_password}

        resp = self.client.post(_resp.wsgi_request.path, payload)

        self.assertRedirects(resp, reverse('accounts:password_reset_complete'))
        self.user.refresh_from_db()
        self.assertFalse(self.user.check_password(self.password))
        self.assertTrue(self.user.check_password(self.new_password))
        message_storage = get_messages(resp.wsgi_request)
        messages = [str(message) for message in message_storage if 'password_reset_confirm' in message.tags.split()]
        self.assertIn(self.success_alert, messages)
