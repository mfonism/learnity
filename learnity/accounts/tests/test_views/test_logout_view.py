from django.contrib.auth import get_user_model
from django.contrib.messages import get_messages
from django.test import TestCase
from django.urls import reverse

from accounts.tests.mixins import UserTestCaseMixin


class TestLogoutView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('logout')
        cls.success_alert = 'You\'ve just logged out of your account.'

    def test_logout(self):
        self.client.force_login(self.user)
        resp = self.client.get(self.url)

        self.assertRedirects(resp, reverse('login'))
        self.assertFalse(resp.wsgi_request.user.is_authenticated)

        message_storage = get_messages(resp.wsgi_request)
        messages = [str(message) for message in message_storage if 'logout' in message.tags.split()]
        self.assertIn(self.success_alert, messages)

    def test_logout_page_content(self):
        self.client.force_login(self.user)
        resp = self.client.get(self.url, follow=True)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/login.html')

        self.assertContains(resp, self.success_alert)
