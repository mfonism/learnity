from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse

from accounts.tests.mixins import UserTestCaseMixin


class TestLoginView(TestCase, UserTestCaseMixin):
    @classmethod
    def setUpTestData(cls):
        super().setUpActivatedUser()
        cls.url = reverse('login')

    def test_get_login_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertTemplateUsed(resp, 'accounts/login.html')
        self.assertContains(resp, 'id_username')
        self.assertContains(resp, 'id_password')
        self.assertContains(resp, 'Log in')

    def test_login(self):
        resp = self.client.post(self.url, {'username': self.email, 'password': self.password})

        self.assertRedirects(resp, reverse('home'))
