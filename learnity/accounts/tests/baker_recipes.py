from model_bakery.recipe import Recipe

from core.utils import ticker


activated_user = Recipe('accounts.User', activated_at=ticker.now, agreed_to_tnc_at=ticker.now, is_active=True)
