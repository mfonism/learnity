from django.contrib.auth import get_user_model
from django.contrib.auth.tokens import default_token_generator
from django.utils import timezone
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode


class UserTestCaseMixin:
    token_generator = default_token_generator

    @classmethod
    def setUpUser(cls, name=None, email=None, password=None, agreed_to_tnc_at=None, is_active=False, activated_at=None):
        cls.name = name or 'Nenn Mich User'
        cls.email = email or 'user@aol.com'
        cls.password = password or 'Dies ist eine Passphrase'
        cls.user = get_user_model().objects.create_user(
            name=cls.name,
            email=cls.email,
            password=cls.password,
            agreed_to_tnc_at=agreed_to_tnc_at or timezone.now(),
            is_active=is_active,
            activated_at=activated_at,
        )

    @classmethod
    def setUpActivatedUser(cls, name=None, email=None, password=None, activated_at=None, agreed_to_tnc_at=None):
        now = timezone.now()
        cls.setUpUser(name=name, email=email, password=password, agreed_to_tnc_at=now, is_active=True, activated_at=now)

    @classmethod
    def get_url_safe_uuid(cls, user=None):
        user = user or cls.user
        return urlsafe_base64_encode(force_bytes(user.pk))

    @classmethod
    def make_token(cls, user=None):
        return cls.token_generator.make_token(user or cls.user)
