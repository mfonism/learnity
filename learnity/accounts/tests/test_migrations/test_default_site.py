from django.conf import settings

from accounts.tests.test_migrations import MigrationsTestCase


class TestDefaultSite(MigrationsTestCase):

    old_behavior_migration = '0001_squashed_0011_auto_20210309_2234'
    new_behavior_migration = '0012_customise_default_site'

    def setUpWithOldBehavior(self, apps):
        SiteModel = apps.get_model('sites', 'Site')
        default_site = SiteModel.objects.get(pk=getattr(settings, 'SITE_ID', 1))

        self.assertEqual(default_site.name, 'example.com')
        self.assertEqual(default_site.domain, 'example.com')

    def test_default_site(self):
        SiteModel = self.apps.get_model('sites', 'Site')
        default_site = SiteModel.objects.get(pk=getattr(settings, 'SITE_ID', 1))

        self.assertEqual(default_site.name, settings.CUSTOM_SITE_NAME)
        self.assertEqual(default_site.domain, settings.CUSTOM_SITE_DOMAIN)
