from django.apps import apps
from django.db import connection
from django.db.migrations.executor import MigrationExecutor
from django.test import TransactionTestCase


class MigrationsTestCase(TransactionTestCase):

    old_behavior_migration = None
    new_behavior_migration = None

    @classmethod
    def assert_attributes(cls):
        assert (
            cls.old_behavior_migration is not None
        ), f'{cls.__name__} must define "old_behavior_migration" attribute.'
        assert (
            cls.new_behavior_migration is not None
        ), f'{cls.__name__} must define "new_behavior_migration" attribute.'

    @classmethod
    def setUpClass(cls):
        cls.assert_attributes()

        super().setUpClass()
        cls.app_name = apps.get_containing_app_config(cls.__module__).name

        cls.old_behavior_migration_target = [(cls.app_name, cls.old_behavior_migration)]
        cls.new_behavior_migration_target = [(cls.app_name, cls.new_behavior_migration)]

    def setUp(self):
        super().setUp()
        executor = MigrationExecutor(connection)

        executor.migrate(self.old_behavior_migration_target)
        old_behavior_apps = executor.loader.project_state(self.old_behavior_migration_target).apps

        self.setUpWithOldBehavior(old_behavior_apps)

        executor.loader.build_graph()  # refresh graph for next migration
        executor.migrate(self.new_behavior_migration_target)
        self.apps = executor.loader.project_state(self.new_behavior_migration_target).apps

    def setUpWithOldBehavior(self, apps):
        pass
