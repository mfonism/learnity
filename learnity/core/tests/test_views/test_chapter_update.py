from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext as _

from model_bakery import baker
from core.tests.utils import get_messages_by_tag


class TestChapterUpdateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has a book with a chapter
        cls.own_book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        cls.own_chapter = baker.make_recipe('core.tests.chapter', book=cls.own_book)

        # update payload
        cls.payload = {
            'number': 4,
            'title': 'Updated title',
            'url': 'https://updated.title.com',
            'description': 'Updated description',
        }

        cls.chapter_update_url = reverse(
            'core:chapter_update', kwargs={'pk': cls.own_chapter.pk, 'book_pk': cls.own_chapter.book_id}
        )
        cls.success_alert = (
            _("You've just updated the chapter: ") + f"<b>{cls.payload['number']}. {cls.payload['title']}</b>"
        )

    def test_GET_redirects_anonymous_user_to_login(self):
        resp = self.client.get(self.chapter_update_url)

        self.assertRedirects(resp, reverse('login') + "?next=" + self.chapter_update_url)

    def test_non_owner_is_forbidden_from_getting_chapter_update_page(self):
        self.client.force_login(self.non_owner)

        resp = self.client.get(self.chapter_update_url)

        self.assertEqual(resp.status_code, 403)

    def test_owner_can_get_chapter_update_page(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.chapter_update_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/chapter_update.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        book_in_context = resp.context['book']
        self.assertEqual(book_in_context, self.own_book)

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.chapter_update_url, self.payload)

        self.assertRedirects(resp, reverse('login') + "?next=" + self.chapter_update_url)

    def test_non_owner_is_forbidden_from_updating_chapter(self):
        self.client.force_login(self.non_owner)

        resp = self.client.post(self.chapter_update_url, self.payload)

        self.assertEqual(resp.status_code, 403)

    def test_owner_can_update_chapter_on_their_book(self):
        self.client.force_login(self.owner)

        resp = self.client.post(self.chapter_update_url, self.payload)

        # success alert message is created
        self.assertIn(self.success_alert, get_messages_by_tag('chapter_update', resp.wsgi_request))

        # user is redirected to chaper_list view for underlying book
        chapter_list_url = reverse('core:book_read', kwargs={'pk': self.own_book.pk}) + '?tab=chapters'
        self.assertRedirects(resp, chapter_list_url)
