from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext as _

from model_bakery import baker

from core.tests.utils import get_messages_by_tag


class TestBookUpdateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')
        # owner has an unpublished book
        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        cls.book_url = reverse('core:book_update', kwargs={'pk': cls.book.pk})
        # owner has a published book
        cls.published_book = baker.make_recipe('core.tests.published_book', created_by=cls.owner)
        cls.published_book_url = reverse('core:book_update', kwargs={'pk': cls.published_book.pk})

        cls.payload = {
            'title': 'Rust By Example (version 2.0)',
            'subtitle': 'Updated with more examples...',
            'abstract': 'Updated abstract with markdown',
            'url': 'https://doc.rust-lang.org/rust-by-example/v2.0/',
            'description': 'Long updated description of RBE (v2.0). In markdown.',
        }
        cls.success_alert = _("You've just updated the book: ") + f"<b>{cls.payload['title']}</b>"

    def test_GET_redirects_anonymous_user_to_login(self):
        resp = self.client.get(self.book_url)

        expected_redirect_url = reverse('login') + "?next=" + self.book_url
        self.assertRedirects(resp, expected_redirect_url)

    def test_non_owner_is_forbidden_from_getting_book_update_page(self):
        self.client.force_login(self.non_owner)

        resp = self.client.get(self.book_url)

        self.assertEqual(resp.status_code, 403)

    def test_owner_can_get_update_page_for_their_book(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.book_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_update.html')
        self.assertTemplateUsed(resp, 'site_base.html')

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.book_url, self.payload)

        expected_redirect_url = reverse('login') + "?next=" + self.book_url
        self.assertRedirects(resp, expected_redirect_url)
        self.book.refresh_from_db()
        # no updates were made
        book = self.book
        payload = self.payload
        self.assertNotEqual(book.title, payload['title'])
        self.assertNotEqual(book.subtitle, payload['subtitle'])
        self.assertNotEqual(book.abstract, payload['abstract'])
        self.assertNotEqual(book.url, payload['url'])
        self.assertNotEqual(book.description, payload['description'])

    def test_non_owner_is_forbidden_from_updating_book(self):
        self.client.force_login(self.non_owner)

        resp = self.client.post(self.book_url, self.payload)

        self.assertEqual(resp.status_code, 403)
        self.book.refresh_from_db()
        # no updates were made
        book = self.book
        payload = self.payload
        self.assertNotEqual(book.title, payload['title'])
        self.assertNotEqual(book.subtitle, payload['subtitle'])
        self.assertNotEqual(book.abstract, payload['abstract'])
        self.assertNotEqual(book.url, payload['url'])
        self.assertNotEqual(book.description, payload['description'])

    def test_owner_can_update_their_unpublished_book(self):
        self.client.force_login(self.owner)

        resp = self.client.post(self.book_url, self.payload)

        expected_success_url = reverse('core:book_read', kwargs={'pk': self.book.pk})
        self.assertRedirects(resp, expected_success_url)
        self.book.refresh_from_db()
        self.assertEqual(self.book.title, self.payload['title'])
        self.assertEqual(self.book.subtitle, self.payload['subtitle'])
        self.assertEqual(self.book.abstract, self.payload['abstract'])
        self.assertEqual(self.book.url, self.payload['url'])
        self.assertEqual(self.book.description, self.payload['description'])

        self.assertIn(self.success_alert, get_messages_by_tag('book_update', resp.wsgi_request))

    def test_title_and_subtitle_of_published_book_do_not_get_updated(self):
        self.client.force_login(self.owner)

        resp = self.client.post(self.published_book_url, self.payload)

        expected_success_url = reverse('core:book_read', kwargs={'pk': self.published_book.pk})
        self.assertRedirects(resp, expected_success_url)
        self.published_book.refresh_from_db()
        self.assertNotEqual(self.published_book.title, self.payload['title'])
        self.assertNotEqual(self.published_book.subtitle, self.payload['subtitle'])
        self.assertEqual(self.published_book.url, self.payload['url'])
        self.assertEqual(self.published_book.description, self.payload['description'])
