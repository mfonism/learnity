from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext as _

from model_bakery import baker


class TestOwnBookListView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has three books
        cls.books = baker.make_recipe('core.tests.book', created_by=cls.owner, _quantity=3)
        # three other books exist that aren't onwer's
        baker.make_recipe('core.tests.book', _quantity=3)

        cls.url = reverse('core:own_book_list')

    def test_unauthenticated_user_is_redirected_to_login(self):
        resp = self.client.get(self.url)

        self.assertRedirects(resp, reverse('login') + '?next=' + self.url)

    def test_list_own_books(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/own_book_list.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        books_in_context = list(resp.context['book_list'])
        self.assertEqual(books_in_context, self.books)
