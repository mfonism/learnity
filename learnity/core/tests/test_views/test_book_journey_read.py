from django.test import TestCase
from django.urls import reverse

from model_bakery import baker


class TestBookJourneyReadView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has a book-journey
        cls.journey = baker.make_recipe('core.tests.book_journey', created_by=cls.owner)
        cls.url = reverse('core:book_journey_read', kwargs={'pk': cls.journey.pk})

    def test_anonymous_user_can_read_book_journey(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_journey_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertEqual(resp.context['book_journey'], self.journey)

    def test_non_owner_can_read_book_journey(self):
        self.client.force_login(self.non_owner)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_journey_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertEqual(resp.context['book_journey'], self.journey)

    def test_owner_can_read_book_journey(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_journey_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertEqual(resp.context['book_journey'], self.journey)
