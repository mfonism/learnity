from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from model_bakery import baker

from core.tests.utils import get_messages_by_tag


class TestBookPublishView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has an unpublished book
        # ...it has chapters
        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        baker.make_recipe('core.tests.chapter', book=cls.book, _quantity=1)
        cls.book_url = reverse('core:book_publish', kwargs={'pk': cls.book.pk})

        # owner has an unpublished book without chapters
        cls.chapterless_book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        cls.chapterless_book_url = reverse('core:book_publish', kwargs={'pk': cls.chapterless_book.pk})

        # owner has a published book
        # ...of course it has chapters
        cls.published_book = baker.make_recipe('core.tests.published_book', created_by=cls.owner)
        baker.make_recipe('core.tests.chapter', book=cls.published_book, _quantity=3)
        cls.published_book_url = reverse('core:book_publish', kwargs={'pk': cls.published_book.pk})

        cls.success_alert = _("You've just published the book") + f': <b>{cls.book.title}</b>'
        cls.duplicate_action_alert = _("You've already published this book!")
        cls.failure_alert = _("You cannot publish this book as it doesn't have any chapters, yet.")

    def test_GET_redirects_user_to_book_read(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.book_url)

        self.assertRedirects(resp, reverse('core:book_read', kwargs={'pk': self.book.pk}))

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.book_url, {})

        self.assertRedirects(resp, reverse('login') + "?next=" + self.book_url)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_published(), False)

    def test_non_owner_is_forbidden_from_publishing_book(self):
        self.client.force_login(self.non_owner)

        resp = self.client.post(self.book_url, {})

        self.assertEqual(resp.status_code, 403)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_published(), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_owner_can_publish_their_book(self, now_mock):
        self.client.force_login(self.owner)

        resp = self.client.post(self.book_url, {})

        self.assertRedirects(resp, reverse('core:book_read', kwargs={'pk': self.book.pk}))
        now_mock.assert_called_once()
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_published(), True)
        self.assertEqual(self.book.published_at, now_mock.return_value)

        self.assertIn(self.success_alert, get_messages_by_tag('book_publish', resp.wsgi_request))

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_owner_cannot_republish_already_published_book(self, now_mock):
        self.client.force_login(self.owner)
        old_publish_date = self.published_book.published_at

        resp = self.client.post(self.published_book_url, {})

        self.assertRedirects(resp, reverse('core:book_read', kwargs={'pk': self.published_book.pk}))
        now_mock.assert_not_called()
        self.book.refresh_from_db()
        self.assertEqual(self.published_book.published_at, old_publish_date)

        self.assertIn(self.duplicate_action_alert, get_messages_by_tag('book_publish_warning', resp.wsgi_request))

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_cannot_publish_book_that_has_no_chapters(self, now_mock):
        self.client.force_login(self.owner)

        resp = self.client.post(self.chapterless_book_url, {})

        self.assertRedirects(
            resp, reverse('core:book_read', kwargs={'pk': self.chapterless_book.pk}) + f'?tab=chapters'
        )
        self.chapterless_book.refresh_from_db()
        self.assertEqual(self.chapterless_book.is_published(), False)

        self.assertIn(self.failure_alert, get_messages_by_tag('book_publish_error', resp.wsgi_request))
