from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from model_bakery import baker

from core.models import Chapter
from core.tests.utils import get_messages_by_tag


class TestChapterCreateVeiw(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)

        cls.chapter_create_url = reverse('core:chapter_create', kwargs={'book_pk': cls.book.pk})

        cls.payload = {'number': 1, 'title': 'Hello World'}
        cls.success_alert = (
            _("You've just created a chapter") + f": <b>{cls.payload['number']}. {cls.payload['title']}</b>"
        )

    def test_GET_redirects_anonymous_user_to_login(self):
        resp = self.client.get(self.chapter_create_url)

        expected_redirect_url = reverse('login') + "?next=" + self.chapter_create_url
        self.assertRedirects(resp, expected_redirect_url)

    def test_non_owner_is_forbidden_from_getting_create_page(self):
        self.client.force_login(self.non_owner)

        resp = self.client.get(self.chapter_create_url)

        self.assertEqual(resp.status_code, 403)

    def test_owner_can_get_create_page(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.chapter_create_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/chapter_create.html')
        self.assertTemplateUsed(resp, 'site_base.html')

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.chapter_create_url, self.payload)

        expected_redirect_url = reverse('login') + "?next=" + self.chapter_create_url
        self.assertRedirects(resp, expected_redirect_url)

    def test_non_owner_is_forbidden_from_creating_chapter_on_book(self):
        self.client.force_login(self.non_owner)

        resp = self.client.post(self.chapter_create_url, self.payload)

        self.assertEqual(resp.status_code, 403)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_owner_can_create_chapter_on_their_book(self, now_mock):
        self.client.force_login(self.owner)
        old_chapter_count = Chapter.objects.count()

        resp = self.client.post(self.chapter_create_url, self.payload)

        now_mock.assert_called_once()
        self.assertEqual(Chapter.objects.count(), old_chapter_count + 1)
        created_chapter = Chapter.objects.latest('created_at')
        self.assertEqual(created_chapter.created_at, now_mock.return_value)
        self.assertEqual(created_chapter.number, self.payload['number'])
        self.assertEqual(created_chapter.title, self.payload['title'])
        self.assertEqual(created_chapter.url, '')
        self.assertEqual(created_chapter.description, '')
        self.assertEqual(created_chapter.book, self.book)

        # success alert message is created
        self.assertIn(self.success_alert, get_messages_by_tag('chapter_create', resp.wsgi_request))

        book_read_url = reverse('core:book_read', kwargs={'pk': self.book.pk})
        expected_redirect_url = book_read_url + '?tab=chapters'
        self.assertRedirects(resp, expected_redirect_url)

    def test_cannot_create_chapter_with_same_number_as_one_on_same_book(self):
        self.client.force_login(self.owner)
        blocking_chapter = baker.make_recipe('core.tests.chapter', number=self.payload['number'], book=self.book)
        old_chapter_count = Chapter.objects.count()

        resp = self.client.post(self.chapter_create_url, self.payload)

        self.assertEqual(Chapter.objects.count(), old_chapter_count)
        self.assertEqual(len(resp.context['form'].errors), 1)
        self.assertTemplateUsed(resp, 'core/chapter_create.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        self.assertEqual(Chapter.objects.count(), old_chapter_count)

    def test_can_create_chapter_with_same_number_as_deleted_one_on_same_book(self):
        self.client.force_login(self.owner)
        non_blocking_chapter = baker.make_recipe('core.tests.chapter', number=self.payload['number'], book=self.book)
        non_blocking_chapter.delete()
        old_chapter_count = Chapter.objects.count()

        resp = self.client.post(self.chapter_create_url, self.payload)

        self.assertEqual(Chapter.objects.count(), old_chapter_count + 1)
        book_read_url = reverse('core:book_read', kwargs={'pk': self.book.pk})
        expected_redirect_url = book_read_url + '?tab=chapters'
        self.assertRedirects(resp, expected_redirect_url)
