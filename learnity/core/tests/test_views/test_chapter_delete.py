from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from model_bakery import baker

from core.tests.utils import get_messages_by_tag


class TestBookDeleteView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has a book with a chapter
        cls.own_book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        chapter_number = 1
        chapter_title = 'Hello World'
        cls.own_chapter = baker.make_recipe(
            'core.tests.chapter', number=chapter_number, title=chapter_title, book=cls.own_book
        )

        cls.chapter_delete_url = reverse(
            'core:chapter_delete', kwargs={'pk': cls.own_chapter.pk, 'book_pk': cls.own_chapter.book_id}
        )
        cls.success_alert = _("You've just deleted the chapter: ") + f"<b>{chapter_number}. {chapter_title}</b>"

    def test_GET_redirects_user_to_chapter_list(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.chapter_delete_url)

        expected_redirect_url = reverse('core:book_read', kwargs={'pk': self.own_book.pk}) + '?tab=chapters'
        self.assertRedirects(resp, expected_redirect_url)

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.chapter_delete_url, {})

        self.assertRedirects(resp, reverse('login') + "?next=" + self.chapter_delete_url)
        self.own_chapter.refresh_from_db()
        self.assertEqual(self.own_chapter.is_deleted(), False)

    def test_non_owner_is_forbidden_from_deleting_chapter(self):
        self.client.force_login(self.non_owner)

        resp = self.client.post(self.chapter_delete_url, {})

        self.assertEqual(resp.status_code, 403)
        self.own_chapter.refresh_from_db()
        self.assertEqual(self.own_chapter.is_deleted(), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_owner_can_delete_chapter_on_their_book(self, now_mock):
        self.client.force_login(self.owner)

        resp = self.client.post(self.chapter_delete_url, {})

        now_mock.assert_called_once()
        self.own_chapter.refresh_from_db()
        chapter_list_url = reverse('core:book_read', kwargs={'pk': self.own_chapter.book_id}) + '?tab=chapters'
        self.assertRedirects(resp, chapter_list_url)

        # success alert message is created
        self.assertIn(self.success_alert, get_messages_by_tag('chapter_delete', resp.wsgi_request))

        self.assertEqual(self.own_chapter.is_deleted(), True)
        self.assertEqual(self.own_chapter.deleted_at, now_mock.return_value)
