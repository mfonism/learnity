from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from model_bakery import baker

from core.models import Book
from core.tests.utils import get_messages_by_tag


class TestBookCreateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = baker.make_recipe('accounts.tests.activated_user')
        cls.book_create_url = reverse('core:book_create')
        cls.payload = {
            'title': 'Rust By Example',
            'url': 'https://doc.rust-lang.org/rust-by-example/',
            'abstract': '255 word short description of the book, RBE',
            'description': 'Long description of RBE. In markdown.',
        }
        cls.success_alert = _("You've just created a book") + f": <b>{cls.payload['title']}</b>"

    def test_GET_redirects_anonymous_user_to_login(self):
        resp = self.client.get(self.book_create_url)

        expected_redirect_url = reverse('login') + '?next=' + self.book_create_url
        self.assertRedirects(resp, expected_redirect_url)

    def test_auth_user_can_GET_book_create_page(self):
        self.client.force_login(self.user)

        resp = self.client.get(self.book_create_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_create.html')
        self.assertTemplateUsed(resp, 'site_base.html')

    def test_POST_redirects_anonymous_user_to_login(self):
        old_book_count = Book.objects.count()

        resp = self.client.post(self.book_create_url, self.payload)

        expected_redirect_url = reverse('login') + '?next=' + self.book_create_url
        self.assertRedirects(resp, expected_redirect_url)
        self.assertEqual(Book.objects.count(), old_book_count)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_auth_user_can_create_book(self, now_mock):
        self.client.force_login(self.user)
        old_book_count = Book.objects.count()

        resp = self.client.post(self.book_create_url, self.payload)

        now_mock.assert_called_once()
        self.assertEqual(Book.objects.count(), old_book_count + 1)

        # created book has the correct values in the correct fields
        created_book = Book.objects.latest('created_at')
        self.assertEqual(created_book.created_at, now_mock.return_value)
        self.assertEqual(created_book.title, self.payload['title'])
        self.assertEqual(created_book.subtitle, '')
        self.assertEqual(created_book.url, self.payload['url'])
        self.assertEqual(created_book.abstract, self.payload['abstract'])
        self.assertEqual(created_book.description, self.payload['description'])
        self.assertEqual(created_book.created_by, self.user)

        # success alert message is created
        self.assertIn(self.success_alert, get_messages_by_tag('book_create', resp.wsgi_request))

        # user is redirected to book_read view for created book
        book_read_url = reverse('core:book_read', kwargs={'pk': created_book.pk})
        self.assertRedirects(resp, book_read_url)
