from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext as _

from model_bakery import baker

from core.tests.utils import get_messages_by_tag


class TestBookDeleteView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        cls.book_delete_url = reverse('core:book_delete', kwargs={'pk': cls.book.pk})

        cls.success_alert = _("You've just deleted the book: ") + f"<b>{cls.book.title}</b>"

    def test_GET_redirects_user_to_book_read(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.book_delete_url)

        self.assertRedirects(resp, reverse('core:book_read', kwargs={'pk': self.book.pk}))

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.book_delete_url, {})

        expected_redirect_url = reverse('login') + "?next=" + self.book_delete_url
        self.assertRedirects(resp, expected_redirect_url)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_deleted(), False)

    def test_non_owner_is_forbidden_from_deleting_book(self):
        self.client.force_login(self.non_owner)

        resp = self.client.post(self.book_delete_url, {})

        self.assertEqual(resp.status_code, 403)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_deleted(), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_owner_can_delete_their_unpublished_book(self, now_mock):
        self.client.force_login(self.owner)

        resp = self.client.post(self.book_delete_url, {})

        now_mock.assert_called_once()
        own_book_list_url = reverse('core:own_book_list')
        self.assertRedirects(resp, own_book_list_url)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_deleted(), True)
        self.assertEqual(self.book.deleted_at, now_mock.return_value)

        # success alert message is created
        self.assertIn(self.success_alert, get_messages_by_tag('book_delete', resp.wsgi_request))

        self.assertEqual(self.book.is_deleted(), True)
        self.assertEqual(self.book.deleted_at, now_mock.return_value)
