from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from model_bakery import baker

from core.models import ChapterJourney


class TestChapterJourneyCreateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # owner and non-owner with respect to book-journey
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        cls.book = baker.make_recipe('core.tests.published_book')
        cls.chapter = baker.make_recipe('core.tests.chapter', number=1, book=cls.book)

        # owner has a book_journey on book
        cls.book_journey = baker.make_recipe('core.tests.book_journey', created_by=cls.owner, book=cls.book)

        cls.url = reverse(
            'core:chapter_journey_create',
            kwargs={'book_journey_pk': cls.book_journey.pk, 'chapter_pk': cls.chapter.pk},
        )

    def test_GET_redirects_to_book_journey_read_view(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.url)

        expected_redirect_url = reverse('core:book_journey_read', kwargs={'pk': self.book_journey.pk})
        self.assertRedirects(resp, expected_redirect_url)

    def test_POST_redirects_anonymous_user_to_login(self):
        resp = self.client.post(self.url, {})

        expected_redirect_url = reverse('login') + '?next=' + self.url
        self.assertRedirects(resp, expected_redirect_url)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_non_owner_is_forbidden_from_creating_chapter_journey(self, now_mock):
        self.client.force_login(self.non_owner)
        old_chapter_journey_count = ChapterJourney.objects.count()

        resp = self.client.post(self.url, {})

        now_mock.assert_not_called()
        self.assertEqual(resp.status_code, 403)
        self.assertEqual(ChapterJourney.objects.count(), old_chapter_journey_count)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_owner_can_create_chapter_journey_on_their_book_journey(self, now_mock):
        self.client.force_login(self.owner)
        old_chapter_journey_count = ChapterJourney.objects.count()

        resp = self.client.post(self.url, {})

        now_mock.assert_called_once()
        self.assertEqual(ChapterJourney.objects.count(), old_chapter_journey_count + 1)
        created_chapter_journey = ChapterJourney.objects.latest('created_at')
        self.assertRedirects(resp, reverse('core:chapter_journey_read', kwargs={'pk': created_chapter_journey.pk}))

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_user_is_forbidden_from_duplicating_chapter_journey(self, now_mock):
        baker.make_recipe('core.tests.chapter_journey', chapter=self.chapter, book_journey=self.book_journey)
        old_chapter_journey_count = ChapterJourney.objects.count()
        self.client.force_login(self.owner)

        resp = self.client.post(self.url, {})

        now_mock.assert_not_called()
        self.assertEqual(resp.status_code, 403)
        self.assertEqual(ChapterJourney.objects.count(), old_chapter_journey_count)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_cannot_create_chapter_journey_with_mismatched_book_journey_and_chapter(self, now_mock):
        other_book = baker.make_recipe('core.tests.published_book')
        other_chapter = baker.make_recipe('core.tests.chapter', number=1, book=other_book)
        url = reverse(
            'core:chapter_journey_create',
            kwargs={'book_journey_pk': self.book_journey.pk, 'chapter_pk': other_chapter.pk},
        )
        self.client.force_login(self.owner)
        old_chapter_journey_count = ChapterJourney.objects.count()

        resp = self.client.post(url, {})

        now_mock.assert_not_called()
        self.assertEqual(resp.status_code, 404)
        self.assertEqual(ChapterJourney.objects.count(), old_chapter_journey_count)
