from django.test import TestCase
from django.urls import reverse

from model_bakery import baker


class TestBookListView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = baker.make_recipe('accounts.tests.activated_user')
        # a number of unpublished books exist in the database
        cls.books = baker.make_recipe('core.tests.book', _quantity=2)
        # a number of published books, too
        cls.books_published = baker.make_recipe('core.tests.published_book', _quantity=3)

        cls.book_list_url = reverse('core:book_list')

    def test_list_books(self):
        resp = self.client.get(self.book_list_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_list.html')
        self.assertTemplateUsed(resp, 'site_base.html')
        # context data is made up of only the published books
        book_list = list(resp.context['book_list'])
        self.assertListEqual(book_list, self.books_published)
