from django.test import TestCase
from django.urls import reverse

from model_bakery import baker


class TestChapterJourneyReadView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has a chapter journey on their published book
        cls.book = baker.make_recipe('core.tests.published_book', created_by=cls.owner)
        cls.chapter = baker.make_recipe('core.tests.chapter', number=1, book=cls.book)
        cls.book_journey = baker.make_recipe('core.tests.book_journey', created_by=cls.owner, book=cls.book)
        cls.chapter_journey = baker.make_recipe(
            'core.tests.chapter_journey', book_journey=cls.book_journey, chapter=cls.chapter
        )

        cls.url = reverse('core:chapter_journey_read', kwargs={'pk': cls.chapter_journey.pk})

    def test_anonymous_user_can_read_chapter_journey(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
