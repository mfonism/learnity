from django.test import TestCase
from django.urls import reverse

from model_bakery import baker


class TestBookReadView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # two users: owner and non_owner
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        # owner has an unpublished book
        # ...it has chapters
        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        baker.make_recipe('core.tests.chapter', book=cls.book, _quantity=1)
        cls.book_url = reverse('core:book_read', kwargs={'pk': cls.book.pk})

        # owner has an unpublished book without chapters
        cls.chapterless_book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        cls.chapterless_book_url = reverse('core:book_read', kwargs={'pk': cls.chapterless_book.pk})

        # owner has a published book
        # ...of course it has chapters
        cls.published_book = baker.make_recipe('core.tests.published_book', created_by=cls.owner)
        baker.make_recipe('core.tests.chapter', book=cls.published_book, _quantity=3)
        cls.published_book_url = reverse('core:book_read', kwargs={'pk': cls.published_book.pk})

    def test_anonymous_user_is_forbidden_from_reading_unpublished_book(self):
        resp = self.client.get(self.book_url)

        self.assertEqual(resp.status_code, 403)

    def test_non_owner_is_forbidden_from_reading_unpublished_book(self):
        self.client.force_login(self.non_owner)

        resp = self.client.get(self.book_url)

        self.assertEqual(resp.status_code, 403)

    def test_owner_can_read_their_unpublished_book(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.book_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')

    def test_anonymous_user_can_read_published_book(self):
        resp = self.client.get(self.published_book_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')

    def test_non_owner_can_read_published_book(self):
        self.client.force_login(self.non_owner)

        resp = self.client.get(self.published_book_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')

    def test_owner_can_read_published_book(self):
        self.client.force_login(self.owner)

        resp = self.client.get(self.published_book_url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/book_read.html')
        self.assertTemplateUsed(resp, 'site_base.html')
