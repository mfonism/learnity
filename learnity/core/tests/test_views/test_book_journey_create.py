from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from model_bakery import baker

from core.models import BookJourney


class TestBookJourneyCreateView(TestCase):
    @classmethod
    def setUpTestData(cls):
        # owner and non-owner with respect to book-journey
        cls.user = baker.make_recipe('accounts.tests.activated_user')

        cls.published_book = baker.make_recipe('core.tests.published_book')
        cls.chapter = baker.make_recipe('core.tests.chapter', number=1, book=cls.published_book)

        cls.url = reverse('core:book_journey_create', kwargs={'book_pk': cls.published_book.pk})

    def test_GET_redirects_to_book_read_view(self):
        self.client.force_login(self.user)

        resp = self.client.get(self.url)

        expected_redirect_url = reverse('core:book_read', kwargs={'pk': self.published_book.pk})
        self.assertRedirects(resp, expected_redirect_url)

    def test_POST_redirects_anonymous_user_to_login(self):
        old_book_journey_count = BookJourney.objects.count()
        resp = self.client.post(self.url, {})

        expected_redirect_url = reverse('login') + '?next=' + self.url
        self.assertRedirects(resp, expected_redirect_url)
        self.assertEqual(BookJourney.objects.count(), old_book_journey_count)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_auth_user_can_create_book_journey(self, now_mock):
        old_book_journey_count = BookJourney.objects.count()
        self.client.force_login(self.user)

        resp = self.client.post(self.url, {})

        now_mock.assert_called_once()
        self.assertEqual(BookJourney.objects.count(), old_book_journey_count + 1)
        created_book_journey = BookJourney.objects.latest('created_at')
        self.assertRedirects(resp, reverse('core:book_journey_read', kwargs={'pk': created_book_journey.pk}))

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_user_is_forbidden_from_creating_book_journey_on_unpublished_book(self, now_mock):
        pass

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_user_is_forbidden_from_duplicating_book_journey(self, now_mock):
        baker.make_recipe('core.tests.book_journey', book=self.published_book, created_by=self.user)
        old_book_journey_count = BookJourney.objects.count()
        self.client.force_login(self.user)

        resp = self.client.post(self.url, {})

        now_mock.assert_not_called()
        self.assertEqual(resp.status_code, 403)
        self.assertEqual(BookJourney.objects.count(), old_book_journey_count)
