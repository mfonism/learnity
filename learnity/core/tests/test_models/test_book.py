from unittest.mock import patch

from django.test import TestCase
from django.utils import timezone

from model_bakery import baker

from core.models import Book, BookJourney, Chapter


class TestBookModel(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')
        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)

    def setUp(self):
        self.book.refresh_from_db()

    def test_string_representation_of_book_contains_its_title(self):
        self.assertEqual(str(self.book).startswith('Book'), True)
        self.assertIn(self.book.title, str(self.book))

    def test_is_owned_by(self):
        self.assertEqual(self.book.is_owned_by(self.owner), True)
        self.assertEqual(self.book.is_owned_by(self.non_owner), False)

    def test_can_be_deleted_by_is_true_for_owner(self):
        self.assertEqual(self.book.can_be_deleted_by(self.book.created_by), True)

    def test_can_be_deleted_by_is_false_for_non_owner(self):
        self.assertEqual(self.book.can_be_deleted_by(self.non_owner), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_delete_is_soft(self, mock_now):
        deletions = self.book.delete()

        mock_now.assert_called_once()
        self.book.refresh_from_db()
        self.assertEqual(self.book.deleted_at, mock_now.return_value)
        self.assertEqual(deletions, (1, {'core.Book': 1}))

    def test_delete_without_committing(self):
        self.book.delete(commit=False)

        self.assertEqual(self.book.is_deleted(), True)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_deleted(), False)

    def test_is_deleted(self):
        self.assertEqual(self.book.is_deleted(), False)

        self.book.delete()

        self.book.refresh_from_db()
        self.assertEqual(self.book.is_deleted(), True)

    def test_deleting_book_deletes_its_chapters(self):
        num_chapter = 3
        chapters = baker.make_recipe('core.tests.chapter', book=self.book, _quantity=num_chapter)
        # already deleted chapter
        baker.make_recipe('core.tests.chapter', book=self.book).delete()

        num_objects_deleted, deletion_info = self.book.delete()

        for chapter in chapters:
            chapter.refresh_from_db()
            with self.subTest(chapter_number=chapter.number):
                self.assertEqual(chapter.is_deleted(), True)
        self.assertEqual(deletion_info, {Book._meta.label: 1, Chapter._meta.label: num_chapter})
        self.assertEqual(num_objects_deleted, num_chapter + 1)

    def test_can_be_published_by_is_true_for_owner(self):
        self.assertEqual(self.book.can_be_published_by(self.book.created_by), True)

    def test_can_be_published_by_is_false_for_non_owner(self):
        self.assertEqual(self.book.can_be_published_by(self.non_owner), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_publish(self, mock_now):
        self.book.publish()

        mock_now.assert_called_once()
        self.book.refresh_from_db()
        self.assertEqual(self.book.published_at, mock_now.return_value)

    def test_publish_without_committing(self):
        self.book.publish(commit=False)

        self.assertEqual(self.book.is_published(), True)
        self.book.refresh_from_db()
        self.assertEqual(self.book.is_published(), False)

    def test_is_published(self):
        self.assertEqual(self.book.is_published(), False)

        self.book.publish()

        self.book.refresh_from_db()
        self.assertEqual(self.book.is_published(), True)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_hard_delete(self, mock_now):
        self.book.delete(hard=True)

        mock_now.assert_not_called()

        self.assertIsNone(self.book.pk)

    def test_has_chapters(self):
        self.assertEqual(self.book.has_chapters(), False)

        baker.make_recipe('core.tests.chapter', book=self.book)

        self.assertEqual(self.book.has_chapters(), True)

    def test_has_chapters_is_false_for_book_whose_chapters_are_deleted(self):
        self.assertEqual(self.book.has_chapters(), False)

        chapter = baker.make_recipe('core.tests.chapter', book=self.book)
        chapter.delete()

        self.assertEqual(self.book.has_chapters(), False)

    def test_get_chapters_gives_only_chapters_that_have_not_been_deleted(self):
        # book has two undeleted chapters
        num_non_deleted_chapters = 2
        non_deleted_chapters = baker.make_recipe(
            'core.tests.chapter', book=self.book, _quantity=num_non_deleted_chapters
        )
        # book has three deleted chapters
        num_deleted_chapters = 3
        deleted_chapters = baker.make_recipe('core.tests.chapter', book=self.book, _quantity=num_deleted_chapters)
        for chapter in deleted_chapters:
            chapter.delete()

        self.assertCountEqual(self.book.get_chapters(), non_deleted_chapters)

    def test_get_book_journeys_gives_only_book_journeys_that_have_not_been_deleted(self):
        non_deleted_book_journeys = baker.make_recipe('core.tests.book_journey', book=self.book, _quantity=2)
        deleted_book_journeys = baker.make_recipe('core.tests.book_journey', book=self.book, _quantity=3)
        for journey in deleted_book_journeys:
            journey.delete()

        self.assertCountEqual(self.book.get_book_journeys(), non_deleted_book_journeys)

    def test_has_book_journeys(self):
        self.assertEqual(self.book.has_book_journeys(), False)

        baker.make_recipe('core.tests.book_journey', book=self.book, _quantity=2)
        self.assertEqual(self.book.has_book_journeys(), True)

    def test_has_book_journey_by_user(self):
        self.assertEqual(self.book.has_book_journey_by_user(self.owner), False)

        book_journey = baker.make_recipe('core.tests.book_journey', book=self.book, created_by=self.owner)
        self.assertEqual(self.book.has_book_journey_by_user(self.owner), True)

        book_journey.delete()
        self.assertEqual(self.book.has_book_journey_by_user(self.owner), False)

    def test_get_book_journey_by_user(self):
        with self.assertRaises(BookJourney.DoesNotExist):
            self.book.get_book_journey_by_user(self.owner)

        baker.make_recipe('core.tests.book_journey', book=self.book, created_by=self.owner)
        book_journey_by_user = self.book.get_book_journey_by_user(self.owner)

        book_journey_by_user.delete()
        with self.assertRaises(BookJourney.DoesNotExist):
            self.book.get_book_journey_by_user(self.owner)
