from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from model_bakery import baker

from core.models import ChapterJourney


class TestChapterJourneyModel(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')
        cls.chapter = baker.make_recipe('core.tests.chapter')

        cls.chapter_journey = baker.make_recipe(
            'core.tests.chapter_journey', chapter=cls.chapter, book_journey__created_by=cls.owner
        )

    def test_is_owned_by(self):
        self.assertEqual(self.chapter_journey.is_owned_by(self.owner), True)
        self.assertEqual(self.chapter_journey.is_owned_by(self.non_owner), False)

    def test_can_be_deleted_by_is_true_for_owner(self):
        self.assertEqual(self.chapter_journey.can_be_deleted_by(self.owner), True)

    def test_can_be_deleted_by_is_false_for_non_owner(self):
        self.assertEqual(self.chapter_journey.can_be_deleted_by(self.non_owner), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_delete_is_soft(self, mock_now):
        deletions = self.chapter_journey.delete()

        mock_now.assert_called_once()
        self.chapter_journey.refresh_from_db()
        self.assertEqual(self.chapter_journey.deleted_at, mock_now.return_value)
        self.assertEqual(deletions, (1, {ChapterJourney._meta.label: 1}))

    def test_delete_without_committing(self):
        self.chapter_journey.delete(commit=False)

        self.assertEqual(self.chapter_journey.is_deleted(), True)
        self.chapter_journey.refresh_from_db()
        self.assertEqual(self.chapter_journey.is_deleted(), False)

    def test_is_deleted(self):
        self.assertEqual(self.chapter_journey.is_deleted(), False)

        self.chapter_journey.delete()

        self.chapter_journey.refresh_from_db()
        self.assertEqual(self.chapter_journey.is_deleted(), True)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_hard_delete(self, mock_now):
        self.chapter_journey.delete(hard=True)

        mock_now.assert_not_called()

        with self.assertRaises(ChapterJourney.DoesNotExist):
            self.chapter_journey.refresh_from_db()

    def test_user_may_have_only_one_chapter_journey_on_a_chapter(self):
        with self.assertRaises(ValidationError):
            chapter_journey = baker.make_recipe(
                'core.tests.chapter_journey', chapter=self.chapter, book_journey__created_by=self.owner
            )
            chapter_journey.full_clean()

        chapter_journey.delete(hard=True)
        self.chapter_journey.delete()

        new_chapter_journey = baker.make_recipe(
            'core.tests.chapter_journey', chapter=self.chapter, book_journey__created_by=self.owner
        )
        new_chapter_journey.full_clean()
