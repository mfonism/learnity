from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from model_bakery import baker

from core.models import Chapter
from core.utils import ticker


class TestChapterModel(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')
        cls.book = baker.make_recipe('core.tests.book', created_by=cls.owner)
        cls.chapter = baker.make_recipe('core.tests.chapter', book=cls.book)
        num_other_chapters = 3
        baker.make_recipe('core.tests.chapter', book=cls.book, _quantity=num_other_chapters)

    def setUp(self):
        self.chapter.refresh_from_db()

    def test_string_representation_of_chapter_contains_its_title(self):
        chapter_str = str(self.chapter)
        self.assertEqual(chapter_str.startswith('Chapter'), True)
        self.assertIn(self.chapter.title, chapter_str)

    def test_can_be_deleted_by_is_true_for_owner(self):
        owner = self.chapter.book.created_by
        self.assertEqual(self.chapter.can_be_deleted_by(owner), True)

    def test_can_be_deleted_by_is_false_for_non_owner(self):
        self.assertEqual(self.chapter.can_be_deleted_by(self.non_owner), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_delete_is_soft(self, mock_now):
        deletions = self.chapter.delete()

        mock_now.assert_called_once()
        self.assertEqual(self.chapter.deleted_at, mock_now.return_value)
        self.assertEqual(deletions, (1, {'core.Chapter': 1}))

    def test_delete_without_committing(self):
        self.chapter.delete(commit=False)

        self.assertEqual(self.chapter.is_deleted(), True)
        self.chapter.refresh_from_db()
        self.assertEqual(self.chapter.is_deleted(), False)

    def test_is_deleted(self):
        self.assertEqual(self.chapter.is_deleted(), False)

        self.chapter.delete()

        self.assertEqual(self.chapter.is_deleted(), True)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_hard_delete(self, mock_now):
        self.chapter.delete(hard=True)

        mock_now.assert_not_called()

        self.assertIsNone(self.chapter.pk)

    def test_chapter_number_must_be_unique_to_book(self):
        with self.assertRaises(ValidationError):
            chapter = Chapter.objects.create(
                number=self.chapter.number, title='a title', book=self.book, created_at=ticker.now()
            )
            chapter.full_clean()

        # chapter number can be same across books
        other_book = baker.make_recipe('core.tests.book', created_by=self.owner)
        chapter = Chapter.objects.create(
            number=self.chapter.number, title='a title', book=other_book, created_at=ticker.now()
        )
        chapter.full_clean()
        self.assertEqual(chapter.number, self.chapter.number)

    def test_chapter_number_uniqueness_only_enforced_on_undeleted_chapters(self):
        self.chapter.delete()

        # new chapter can be created with deleted chapter's number
        new_chapter = Chapter.objects.create(
            number=self.chapter.number, title='a title', book=self.book, created_at=ticker.now()
        )
        new_chapter.full_clean()
        self.assertEqual(self.chapter.number, new_chapter.number)
