from unittest.mock import patch

from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from model_bakery import baker

from core.models import BookJourney, ChapterJourney


class TestBookModel(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.owner = baker.make_recipe('accounts.tests.activated_user')
        cls.non_owner = baker.make_recipe('accounts.tests.activated_user')

        cls.book_journey = baker.make_recipe('core.tests.book_journey', created_by=cls.owner)

    def setUp(self):
        self.book_journey.refresh_from_db()

    def test_is_owned_by(self):
        self.assertEqual(self.book_journey.is_owned_by(self.owner), True)
        self.assertEqual(self.book_journey.is_owned_by(self.non_owner), False)

    def test_can_be_deleted_by_is_true_for_owner(self):
        self.assertEqual(self.book_journey.can_be_deleted_by(self.owner), True)

    def test_can_be_deleted_by_is_false_for_non_owner(self):
        self.assertEqual(self.book_journey.can_be_deleted_by(self.non_owner), False)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_delete_is_soft(self, mock_now):
        deletions = self.book_journey.delete()

        mock_now.assert_called_once()
        self.book_journey.refresh_from_db()
        self.assertEqual(self.book_journey.deleted_at, mock_now.return_value)
        self.assertEqual(deletions, (1, {BookJourney._meta.label: 1}))

    def test_delete_without_committing(self):
        self.book_journey.delete(commit=False)

        self.assertEqual(self.book_journey.is_deleted(), True)
        self.book_journey.refresh_from_db()
        self.assertEqual(self.book_journey.is_deleted(), False)

    def test_is_deleted(self):
        self.assertEqual(self.book_journey.is_deleted(), False)

        self.book_journey.delete()

        self.book_journey.refresh_from_db()
        self.assertEqual(self.book_journey.is_deleted(), True)

    def test_deleting_book_journey_deletes_its_chapter_journeys(self):
        num_chapter_journeys = 3
        chapter_journeys = baker.make_recipe(
            'core.tests.chapter_journey', book_journey=self.book_journey, _quantity=num_chapter_journeys
        )
        # already deleted chapter journey
        baker.make_recipe('core.tests.chapter_journey', book_journey=self.book_journey).delete()

        num_objects_deleted, deletion_info = self.book_journey.delete()

        for chapter_journey in chapter_journeys:
            chapter_journey.refresh_from_db()
            with self.subTest():
                self.assertEqual(chapter_journey.is_deleted(), True)
        self.assertEqual(deletion_info, {BookJourney._meta.label: 1, ChapterJourney._meta.label: num_chapter_journeys})
        self.assertEqual(num_objects_deleted, num_chapter_journeys + 1)

    @patch('core.utils.ticker.now', autospec=True, return_value=timezone.now())
    def test_hard_delete(self, mock_now):
        self.book_journey.delete(hard=True)

        mock_now.assert_not_called()

        with self.assertRaises(BookJourney.DoesNotExist):
            self.book_journey.refresh_from_db()

    def test_has_chapter_journeys(self):
        self.assertEqual(self.book_journey.has_chapter_journeys(), False)

        chapter_journey = baker.make_recipe('core.tests.chapter_journey', book_journey=self.book_journey)
        self.assertEqual(self.book_journey.has_chapter_journeys(), True)

        chapter_journey.delete()
        self.assertEqual(self.book_journey.has_chapter_journeys(), False)

    def test_get_chapter_journeys(self):
        self.assertEqual(self.book_journey.get_chapter_journeys().count(), 0)

        chapter_journeys = baker.make_recipe('core.tests.chapter_journey', book_journey=self.book_journey, _quantity=3)
        other_chapter_journeys = baker.make_recipe(
            'core.tests.chapter_journey', book_journey=self.book_journey, _quantity=2
        )
        all_chapter_journeys = [*chapter_journeys, *other_chapter_journeys]
        self.assertCountEqual(self.book_journey.get_chapter_journeys(), all_chapter_journeys)

        for chapter_journey in other_chapter_journeys:
            chapter_journey.delete()
        self.assertCountEqual(self.book_journey.get_chapter_journeys(), chapter_journeys)

    def test_has_chapter_journey_for_chapter(self):
        chapter = baker.make_recipe('core.tests.chapter')
        self.assertEqual(self.book_journey.has_chapter_journey_for_chapter(chapter), False)

        chapter_journey = baker.make_recipe(
            'core.tests.chapter_journey', book_journey=self.book_journey, chapter=chapter
        )
        self.assertEqual(self.book_journey.has_chapter_journey_for_chapter(chapter), True)

        chapter_journey.delete()
        self.assertEqual(self.book_journey.has_chapter_journey_for_chapter(chapter), False)

    def test_get_chapter_journey_for_chapter(self):
        chapter = baker.make_recipe('core.tests.chapter')
        with self.assertRaises(ChapterJourney.DoesNotExist):
            self.book_journey.get_chapter_journey_for_chapter(chapter)

        chapter_journey = baker.make_recipe(
            'core.tests.chapter_journey', book_journey=self.book_journey, chapter=chapter
        )
        self.assertEqual(self.book_journey.get_chapter_journey_for_chapter(chapter), chapter_journey)

        chapter_journey.delete()
        new_chapter_journey = baker.make_recipe(
            'core.tests.chapter_journey', book_journey=self.book_journey, chapter=chapter
        )
        self.assertEqual(self.book_journey.get_chapter_journey_for_chapter(chapter), new_chapter_journey)

    def test_user_may_have_only_one_book_journey_on_a_book(self):
        book = self.book_journey.book
        with self.assertRaises(ValidationError):
            book_journey = baker.make_recipe('core.tests.book_journey', book=book, created_by=self.owner)
            book_journey.full_clean()

        book_journey.delete(hard=True)
        self.book_journey.delete()

        new_book_journey = baker.make_recipe('core.tests.book_journey', book=book, created_by=self.owner)
        new_book_journey.full_clean()
        self.assertEqual(book.get_book_journey_by_user(self.owner), new_book_journey)
