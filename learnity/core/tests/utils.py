from django.contrib.messages import get_messages


def get_messages_by_tag(tag, request):
    return [str(message) for message in get_messages(request) if tag in message.tags.split()]
