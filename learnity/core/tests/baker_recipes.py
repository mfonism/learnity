from model_bakery.recipe import Recipe, foreign_key

from core.utils import ticker

# books
book = Recipe(
    'core.Book',
    created_at=ticker.now,
    created_by=foreign_key('accounts.tests.activated_user'),
)
published_book = book.extend(published_at=ticker.now)
deleted_book = book.extend(deleted_at=ticker.now)

# chapters
chapter = Recipe('core.Chapter', created_at=ticker.now, book=foreign_key('core.tests.book'))

# book-journeys
book_journey = Recipe(
    'core.BookJourney',
    created_at=ticker.now,
    created_by=foreign_key('accounts.tests.activated_user'),
    book=foreign_key('core.tests.published_book'),
)

# chapter-journeys
chapter_journey = Recipe(
    'core.ChapterJourney',
    created_at=ticker.now,
    book_journey=foreign_key('core.tests.book_journey'),
    chapter=foreign_key('core.tests.chapter'),
)
