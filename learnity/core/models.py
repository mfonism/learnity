import datetime
import uuid
from collections import defaultdict
from typing import Optional

from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from . import utils


class Book(models.Model):

    _meta: models.options.Options
    abstract: str
    book_journey_set: models.manager.Manager
    chapter_set: models.manager.Manager
    created_at: datetime.datetime
    created_by_id: int
    deleted_at: Optional[datetime.datetime]
    description: str
    objects: models.manager.Manager
    published_at: Optional[datetime.datetime]
    subtitle: Optional[str]
    title: str
    url: str

    title = models.CharField(_('title'), max_length=64)
    subtitle = models.CharField(_('subtitle'), max_length=128, blank=True)
    abstract = models.CharField(_('abstract'), max_length=255)
    url = models.URLField(_('url'))
    description = models.TextField(_('description'))
    created_at = models.DateTimeField(_('creation timestamp'), blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('creator'), blank=True, on_delete=models.PROTECT
    )
    published_at = models.DateTimeField(_('publication timestamp'), blank=True, null=True)
    deleted_at = models.DateTimeField(_('deletion timestamp'), blank=True, null=True)

    def __str__(self):
        return f'Book: {self.title}'

    def get_absolute_url(self):
        return reverse('core:book_read', kwargs={'pk': self.pk})

    def is_owned_by(self, user):
        return self.created_by_id == user.pk

    def can_be_deleted_by(self, user):
        return self.is_owned_by(user)

    def delete(self, *args, **kwargs):
        if kwargs.get('hard', False):
            return super().delete()

        self.deleted_at = utils.ticker.now()
        if kwargs.get('commit', True):
            self.save()

        # delete chapters and compile deletion info for return
        deletion_info = defaultdict(int)
        deletion_info[self._meta.label] += 1
        for chapter in self.get_chapters():
            _, info = chapter.delete(*args, **kwargs)
            for k, v in info.items():
                deletion_info[k] += v

        return sum(deletion_info.values()), dict(deletion_info)

    def is_deleted(self):
        return self.deleted_at is not None

    def can_be_published_by(self, user):
        return self.is_owned_by(user)

    def publish(self, *args, **kwargs):
        self.published_at = utils.ticker.now()
        if kwargs.get('commit', True):
            self.save()

    def is_published(self):
        return self.published_at is not None

    def has_chapters(self):
        return self.get_chapters().exists()

    def get_chapters(self):
        return self.chapter_set.filter(deleted_at__isnull=True)

    def get_book_journeys(self):
        return self.book_journey_set.filter(deleted_at__isnull=True)

    def has_book_journeys(self):
        return self.get_book_journeys().exists()

    def _get_book_journeys_by_user(self, user):
        return self.get_book_journeys().filter(created_by_id=user.pk)

    def has_book_journey_by_user(self, user):
        return self._get_book_journeys_by_user(user).exists()

    def get_book_journey_by_user(self, user):
        return self._get_book_journeys_by_user(user).get()


class Chapter(models.Model):

    _meta: models.options.Options
    book: Book
    book_id: int
    created_at: datetime.datetime
    deleted_at: Optional[datetime.datetime]
    description: str
    number: int
    objects: models.manager.Manager
    title: str
    url: Optional[str]

    number = models.PositiveSmallIntegerField(_('chapter number'))
    title = models.CharField(_('chapter title'), max_length=64)
    url = models.URLField(_('chapter URL'), blank=True)
    description = models.TextField(_('chapter description'), blank=True)
    created_at = models.DateTimeField(_('creation timestamp'), blank=True)
    deleted_at = models.DateTimeField(_('deletion timestamp'), blank=True, null=True)
    book = models.ForeignKey('core.Book', verbose_name=_('book'), blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return f'Chapter: {self.title}'

    def clean(self):
        super().clean()
        if self.book_id is None:
            return
        for chapter in self.book.get_chapters():
            if chapter.pk != self.pk and chapter.number == self.number:
                raise ValidationError(_('A chapter with this number already exists on the same book'), code='invalid')

    def get_absolute_url(self):
        return reverse('core:book_read', kwargs={'pk': self.book_id}) + '?tab=chapters'

    def can_be_deleted_by(self, user):
        return self.book.is_owned_by(user)

    def delete(self, *args, **kwargs):
        if kwargs.get('hard', False):
            return super().delete()

        self.deleted_at = utils.ticker.now()
        if kwargs.get('commit', True):
            self.save()

        return 1, {self._meta.label: 1}

    def is_deleted(self):
        return self.deleted_at is not None


class BookJourney(models.Model):

    _meta: models.options.Options
    book: Book
    book_id: int
    chapter_journey_set: models.manager.Manager
    created_at: datetime.datetime
    created_by_id: int
    deleted_at: Optional[datetime.datetime]
    objects: models.manager.Manager
    uuid: str

    class Meta:
        verbose_name = 'book journey'

    uuid = models.UUIDField(_('UUID'), default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    book = models.ForeignKey(
        'core.Book', verbose_name=_('book'), blank=True, on_delete=models.PROTECT, related_name='book_journey_set'
    )
    created_at = models.DateTimeField(_('creation timestamp'), blank=True)
    created_by = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('creator'), blank=True, on_delete=models.PROTECT
    )
    deleted_at = models.DateTimeField(_('deletion timestamp'), blank=True, null=True)

    def clean(self):
        super().clean()
        if self.book_id is None or self.created_by_id is None:
            return
        if BookJourney.objects.filter(
            ~Q(pk=self.pk), deleted_at__isnull=True, book_id=self.book_id, created_by_id=self.created_by_id
        ).exists():
            raise ValidationError(_('This user already has a book journey on the underlying book'), code='invalid')

    def can_be_deleted_by(self, user):
        return user.pk == self.created_by_id

    def delete(self, *args, **kwargs):
        if kwargs.get('hard', False):
            return super().delete()

        self.deleted_at = utils.ticker.now()
        if kwargs.get('commit', True):
            self.save()

        # delete chapter-journeys and compile deletion info for return
        deletion_info = defaultdict(int)
        deletion_info[self._meta.label] += 1
        for chapter_journey in self.get_chapter_journeys():
            _, info = chapter_journey.delete(*args, **kwargs)
            for k, v in info.items():
                deletion_info[k] += v

        return sum(deletion_info.values()), dict(deletion_info)

    def is_deleted(self):
        return self.deleted_at is not None

    def get_absolute_url(self):
        return reverse('core:book_journey_read', kwargs={'pk': self.pk})

    def is_owned_by(self, user):
        return self.created_by_id == user.pk

    def has_chapter_journeys(self):
        return self.get_chapter_journeys().exists()

    def get_chapter_journeys(self):
        return self.chapter_journey_set.filter(deleted_at__isnull=True)

    def _get_chapter_journeys_for_chapter(self, chapter: Chapter):
        return self.get_chapter_journeys().filter(chapter_id=chapter.pk)

    def has_chapter_journey_for_chapter(self, chapter: Chapter):
        return self._get_chapter_journeys_for_chapter(chapter).exists()

    def get_chapter_journey_for_chapter(self, chapter: Chapter):
        return self._get_chapter_journeys_for_chapter(chapter).get()


class ChapterJourney(models.Model):

    _meta: models.options.Options
    book_journey: BookJourney
    book_journey_id: str
    chapter: Chapter
    chapter_id: int
    created_at: datetime.datetime
    deleted_at: Optional[datetime.datetime]
    objects: models.manager.Manager
    uuid: str

    class Meta:
        verbose_name = 'chapter journey'

    uuid = models.UUIDField(_('UUID'), default=uuid.uuid4, unique=True, primary_key=True, editable=False)
    book_journey = models.ForeignKey(
        'core.BookJourney',
        verbose_name=_('book journey'),
        blank=True,
        on_delete=models.PROTECT,
        related_name='chapter_journey_set',
    )
    chapter = models.ForeignKey('core.Chapter', verbose_name=_('chapter'), blank=True, on_delete=models.PROTECT)
    created_at = models.DateTimeField(_('creation timestamp'), blank=True)
    deleted_at = models.DateTimeField(_('deletion timestamp'), blank=True, null=True)

    def clean(self):
        super().clean()
        if self.book_journey_id is None or self.chapter_id is None:
            return
        if ChapterJourney.objects.filter(
            ~Q(pk=self.pk),
            deleted_at__isnull=True,
            book_journey__created_by_id=self.book_journey.created_by_id,
            chapter_id=self.chapter_id,
        ).exists():
            raise ValidationError(
                _('This user already has a chapter journey on the underlying chapter'), code='invalid'
            )

    def get_absolute_url(self):
        return reverse('core:chapter_journey_read', kwargs={'pk': self.pk})

    def is_owned_by(self, user):
        return self.book_journey.is_owned_by(user)

    def can_be_deleted_by(self, user):
        return self.is_owned_by(user)

    def delete(self, *args, **kwargs):
        if kwargs.get('hard', False):
            return super().delete()

        self.deleted_at = utils.ticker.now()
        if kwargs.get('commit', True):
            self.save()

        return 1, {self._meta.label: 1}

    def is_deleted(self):
        return self.deleted_at is not None
