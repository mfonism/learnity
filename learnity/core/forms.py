from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from .models import Chapter


class ChapterCreateForm(forms.ModelForm):
    class Meta:
        fields = ['number', 'title', 'url', 'description']
        model = Chapter

    def __init__(self, *args, **kwargs):
        self.book = kwargs.pop('book')
        super().__init__(*args, **kwargs)

    def clean_number(self):
        cleaned_number = self.cleaned_data['number']
        for chapter in self.book.get_chapters():
            if chapter.number == cleaned_number:
                raise ValidationError(_('A chapter with this number already exists on the same book'), code='invalid')
        return cleaned_number
