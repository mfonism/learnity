# Generated by Django 3.2.3 on 2021-07-18 00:39

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0026_chapterjourney_deleted_at'),
    ]

    operations = [
        migrations.AlterField(
            model_name='chapterjourney',
            name='book_journey',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.PROTECT, related_name='chapter_journey_set', to='core.bookjourney', verbose_name='book journey'),
        ),
    ]
