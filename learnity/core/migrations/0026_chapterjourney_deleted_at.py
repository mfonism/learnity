# Generated by Django 3.2.3 on 2021-07-17 15:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0025_chapterjourney_chapter'),
    ]

    operations = [
        migrations.AddField(
            model_name='chapterjourney',
            name='deleted_at',
            field=models.DateTimeField(blank=True, null=True, verbose_name='deletion timestamp'),
        ),
    ]
