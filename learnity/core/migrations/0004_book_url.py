# Generated by Django 3.2.3 on 2021-06-09 22:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_book_subtitle'),
    ]

    operations = [
        migrations.AddField(
            model_name='book',
            name='url',
            field=models.URLField(default='', verbose_name='url'),
            preserve_default=False,
        ),
    ]
