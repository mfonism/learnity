from django import forms
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.utils.html import format_html, mark_safe
from django.utils.translation import gettext_lazy as _
from django.views.generic import CreateView, DeleteView, DetailView, ListView, UpdateView, View

from .forms import ChapterCreateForm
from .models import Book, BookJourney, Chapter, ChapterJourney
from .utils import ticker


class BookCreateView(LoginRequiredMixin, CreateView):
    fields = ['title', 'subtitle', 'abstract', 'url', 'description']
    model = Book
    template_name = 'core/book_create.html'

    def form_valid(self, form):
        book = form.save(commit=False)
        book.created_at = ticker.now()
        book.created_by = self.request.user
        book.save()
        messages.success(
            self.request,
            format_html("{}: <b>{}</b>", mark_safe(_("You've just created a book")), book.title),
            extra_tags='book_create',
        )
        return HttpResponseRedirect(book.get_absolute_url())

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['abstract'].widget = forms.Textarea(
            attrs={'rows': 4, 'data-maxlength': form.fields['abstract'].max_length}
        )
        form.fields['description'].widget.attrs.update(rows=6)
        return form


class BookReadView(DetailView):
    queryset = Book.objects.filter(deleted_at__isnull=True).prefetch_related(
        'chapter_set', 'book_journey_set', 'created_by'
    )
    template_name = 'core/book_read.html'

    def get_object(self, queryset=None):
        book = super().get_object(queryset)
        if not book.is_published() and not book.can_be_published_by(self.request.user):
            raise PermissionDenied
        return book

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['active_tab'] = self.request.GET.get('tab', 'summary')
        return data


class OwnBookListView(LoginRequiredMixin, ListView):
    template_name = 'core/own_book_list.html'

    def get_queryset(self):
        return Book.objects.filter(deleted_at__isnull=True).filter(created_by_id=self.request.user.pk)


class BookListView(ListView):
    queryset = (
        Book.objects.filter(deleted_at__isnull=True).filter(published_at__isnull=False).select_related('created_by')
    )


class BookUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['title', 'subtitle', 'abstract', 'url', 'description']
    queryset = Book.objects.filter(deleted_at__isnull=True)
    template_name = 'core/book_update.html'

    def get_object(self, queryset=None):
        book = super().get_object(queryset)
        if not book.is_owned_by(self.request.user):
            raise PermissionDenied
        return book

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['abstract'].widget = forms.Textarea(
            attrs={'rows': 4, 'data-maxlength': form.fields['abstract'].max_length}
        )
        form.fields['description'].widget.attrs.update(rows=6)
        if self.object.is_published():
            form.fields['title'].disabled = True
            form.fields['subtitle'].disabled = True
        return form

    def form_valid(self, form):
        book = form.save()
        messages.success(
            self.request,
            format_html("{}: <b>{}</b>", mark_safe(_("You've just updated the book")), book.title),
            extra_tags='book_update',
        )
        return HttpResponseRedirect(book.get_absolute_url())


class BookPublishView(LoginRequiredMixin, UpdateView):
    fields = []
    queryset = Book.objects.filter(deleted_at__isnull=True)

    def get_object(self, queryset=None):
        book = super().get_object(queryset)
        if not book.can_be_published_by(self.request.user):
            raise PermissionDenied
        return book

    def get(self, request, *args, **kwargs):
        book = self.get_object()
        return HttpResponseRedirect(book.get_absolute_url())

    def post(self, request, *args, **kwargs):
        book = self.get_object()
        is_book_chapterless = not book.has_chapters()
        if book.is_published():
            messages.warning(
                self.request,
                mark_safe(_("You've already published this book!")),
                extra_tags='book_publish book_publish_warning',
            )
        else:
            if is_book_chapterless:
                messages.error(
                    self.request,
                    mark_safe(_("You cannot publish this book as it doesn't have any chapters, yet.")),
                    extra_tags='book_publish_error',
                )
            else:
                book.publish()
                messages.success(
                    self.request,
                    format_html("{}: <b>{}</b>", mark_safe(_("You've just published the book")), book.title),
                    extra_tags='book_publish',
                )

        success_url = book.get_absolute_url() + ('?tab=chapters' if is_book_chapterless else '')
        return HttpResponseRedirect(success_url)


class BookDeleteView(LoginRequiredMixin, DeleteView):
    queryset = Book.objects.filter(deleted_at__isnull=True)
    success_url = reverse_lazy('core:own_book_list')

    def get_object(self, queryset=None):
        book = super().get_object(queryset)
        if not book.can_be_deleted_by(self.request.user):
            raise PermissionDenied
        return book

    def get(self, request, *args, **kwargs):
        book = self.get_object()
        return HttpResponseRedirect(book.get_absolute_url())

    def delete(self, request, *args, **kwargs):
        book = self.get_object()
        book.delete()
        messages.success(
            self.request,
            format_html(
                "{}: <b>{}</b>",
                mark_safe(_("You've just deleted the book")),
                book.title,
            ),
            extra_tags='book_delete',
        )
        return HttpResponseRedirect(self.success_url)


class ChapterCreateView(LoginRequiredMixin, CreateView):
    form_class = ChapterCreateForm
    model = Chapter
    pk_url_kwarg = 'book_pk'
    template_name = 'core/chapter_create.html'

    def get_object(self, queryset=None):
        book = super().get_object(queryset=queryset)
        if not book.is_owned_by(self.request.user):
            raise PermissionDenied
        return book

    def get(self, request, *args, **kwargs):
        self.book = self.get_object(queryset=self.get_book_queryset())
        return super().get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.book = self.get_object(queryset=self.get_book_queryset())
        return super().post(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['book'] = self.book
        return kwargs

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['book'] = self.book
        return data

    def form_valid(self, form):
        chapter = form.save(commit=False)
        chapter.created_at = ticker.now()
        chapter.book = self.book
        chapter.save()
        messages.success(
            self.request,
            format_html(
                "{}: <b>{}. {}</b>",
                mark_safe(_("You've just created a chapter")),
                chapter.number,
                chapter.title,
            ),
            extra_tags='chapter_create',
        )
        return HttpResponseRedirect(chapter.get_absolute_url())

    def get_book_queryset(self):
        return Book.objects.filter(deleted_at__isnull=True)


class ChapterUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['number', 'title', 'url', 'description']
    queryset = Chapter.objects.filter(deleted_at__isnull=True).select_related('book')
    template_name = 'core/chapter_update.html'

    def get_object(self, queryset=None):
        chapter = super().get_object(queryset=queryset)
        self.book = chapter.book
        if not chapter.book.is_owned_by(self.request.user):
            raise PermissionDenied
        return chapter

    def get_context_data(self, **kwargs):
        data = super().get_context_data(**kwargs)
        data['book'] = self.book
        return data

    def form_valid(self, form):
        chapter = form.save()
        messages.success(
            self.request,
            format_html(
                "{}: <b>{}. {}</b>",
                mark_safe(_("You've just updated the chapter")),
                chapter.number,
                chapter.title,
            ),
            extra_tags='chapter_update',
        )
        return HttpResponseRedirect(chapter.get_absolute_url())


class ChapterDeleteView(LoginRequiredMixin, DeleteView):
    queryset = Chapter.objects.filter(deleted_at__isnull=True).select_related('book')

    def get_object(self, queryset=None):
        chapter = super().get_object(queryset)
        if not chapter.can_be_deleted_by(self.request.user):
            raise PermissionDenied
        return chapter

    def get(self, request, *args, **kwargs):
        chapter = self.get_object()
        return HttpResponseRedirect(chapter.get_absolute_url())

    def delete(self, request, *args, **kwargs):
        chapter = self.get_object()
        chapter.delete()
        messages.success(
            self.request,
            format_html(
                "{}: <b>{}. {}</b>",
                mark_safe(_("You've just deleted the chapter")),
                chapter.number,
                chapter.title,
            ),
            extra_tags='chapter_delete',
        )
        success_url = chapter.book.get_absolute_url() + '?tab=chapters'
        return HttpResponseRedirect(success_url)


class BookJourneyCreateView(LoginRequiredMixin, CreateView):
    fields = []
    model = BookJourney
    pk_url_kwarg = 'book_pk'

    def form_valid(self, form):
        book_journey = form.save(commit=False)
        book_journey.created_at = ticker.now()
        book_journey.book = self.book
        book_journey.created_by = self.request.user
        book_journey.save()
        return HttpResponseRedirect(book_journey.get_absolute_url())

    def get(self, request, *args, **kwargs):
        book = self.get_object(queryset=self.get_book_queryset())
        return HttpResponseRedirect(book.get_absolute_url())

    def post(self, request, *args, **kwargs):
        self.book = self.get_object(queryset=self.get_book_queryset())
        if self.book.has_book_journey_by_user(self.request.user):
            raise PermissionDenied
        return super().post(request, *args, **kwargs)

    def get_book_queryset(self):
        return Book.objects.filter(deleted_at__isnull=True).prefetch_related('book_journey_set')


class BookJourneyReadView(DetailView):
    context_object_name = 'book_journey'
    queryset = BookJourney.objects.filter(deleted_at__isnull=True).prefetch_related(
        'book', 'book__chapter_set', 'chapter_journey_set', 'created_by'
    )
    template_name = 'core/book_journey_read.html'


class ChapterJourneyCreateView(LoginRequiredMixin, CreateView):
    fields = []
    model = ChapterJourney

    def form_valid(self, form):
        chapter_journey = form.save(commit=False)
        chapter_journey.created_at = ticker.now()
        chapter_journey.book_journey = self.book_journey
        chapter_journey.chapter = self.chapter
        chapter_journey.save()
        return HttpResponseRedirect(chapter_journey.get_absolute_url())

    def get(self, request, *args, **kwargs):
        self.pk_url_kwarg = 'book_journey_pk'
        book_journey = self.get_object(queryset=self.get_book_journey_queryset())
        return HttpResponseRedirect(book_journey.get_absolute_url())

    def post(self, request, *args, **kwargs):
        self.pk_url_kwarg = 'book_journey_pk'
        self.book_journey = self.get_object(queryset=self.get_book_journey_queryset())
        if not self.book_journey.is_owned_by(request.user):
            raise PermissionDenied

        self.pk_url_kwarg = 'chapter_pk'
        self.chapter = self.get_object(queryset=self.book_journey.book.get_chapters())
        if self.book_journey.has_chapter_journey_for_chapter(self.chapter):
            raise PermissionDenied

        return super().post(request, *args, **kwargs)

    def get_book_journey_queryset(self):
        return BookJourney.objects.filter(deleted_at__isnull=True).prefetch_related(
            'book', 'book__chapter_set', 'chapter_journey_set'
        )


class ChapterJourneyReadView(DetailView):
    queryset = ChapterJourney.objects.filter(deleted_at__isnull=True).select_related('book_journey', 'chapter')
