from django.urls import path

from core.views import (
    BookCreateView,
    BookDeleteView,
    BookJourneyCreateView,
    BookJourneyReadView,
    BookListView,
    BookPublishView,
    BookReadView,
    BookUpdateView,
    ChapterCreateView,
    ChapterDeleteView,
    ChapterJourneyCreateView,
    ChapterJourneyReadView,
    ChapterUpdateView,
    OwnBookListView,
)

app_name = 'core'
urlpatterns = [
    # books
    path('books/', BookListView.as_view(), name='book_list'),
    path('books/own/', OwnBookListView.as_view(), name='own_book_list'),
    path('books/add', BookCreateView.as_view(), name='book_create'),
    path('books/<int:pk>/', BookReadView.as_view(), name='book_read'),
    path('books/<int:pk>/edit', BookUpdateView.as_view(), name='book_update'),
    path('books/<int:pk>/publish', BookPublishView.as_view(), name='book_publish'),
    path('books/<int:pk>/delete', BookDeleteView.as_view(), name='book_delete'),
    # chapters
    path('books/<int:book_pk>/chapters/add', ChapterCreateView.as_view(), name='chapter_create'),
    path('books/<int:book_pk>/chapters/<int:pk>/edit', ChapterUpdateView.as_view(), name='chapter_update'),
    path('books/<int:book_pk>/chapters/<int:pk>/delete', ChapterDeleteView.as_view(), name='chapter_delete'),
    # book-journeys
    path('book/<int:book_pk>/start-journey', BookJourneyCreateView.as_view(), name='book_journey_create'),
    path('book-journeys/<uuid:pk>/', BookJourneyReadView.as_view(), name='book_journey_read'),
    # chapter-journeys
    path(
        'book-journeys/<uuid:book_journey_pk>/chapters/<int:chapter_pk>/start-journey',
        ChapterJourneyCreateView.as_view(),
        name='chapter_journey_create',
    ),
    path('chapter-journeys/<uuid:pk>/', ChapterJourneyReadView.as_view(), name='chapter_journey_read'),
]
