from django import template

register = template.Library()


@register.filter
def is_owner_of_book(user, book):
    return book.is_owned_by(user)


@register.filter
def is_owner_of_book_journey(user, journey):
    return journey.is_owned_by(user)


@register.filter
def can_delete_book(user, book):
    return book.can_be_deleted_by(user)


@register.filter
def can_publish_book(user, book):
    return book.can_be_published_by(user)


@register.filter
def to_set(iterable):
    return set(iterable)


@register.filter
def order_by(queryset, field_name):
    return queryset.order_by(field_name)


@register.filter
def has_book_journey_by_user(book, user):
    return book.has_book_journey_by_user(user)


@register.filter
def get_book_journey_by_user(book, user):
    return book.get_book_journey_by_user(user)


@register.filter
def has_chapter_journey_for_chapter(book_journey, chapter):
    return book_journey.has_chapter_journey_for_chapter(chapter)


@register.filter
def get_chapter_journey_for_chapter(book_journey, chapter):
    return book_journey.get_chapter_journey_for_chapter(chapter)
