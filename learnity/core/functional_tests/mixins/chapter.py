from typing import Callable, List

from playwright.sync_api._generated import BrowserContext, Page

from django.urls import reverse
from django.utils.translation import gettext as _

from core.models import Book, Chapter


class ChapterStoryPagesMixin:

    get_absolute_url: Callable[[str], str]

    # messages
    msg_forbidden = _('403 Forbidden')

    def get_chapter_create_path(self, book: Book) -> str:
        return reverse('core:chapter_create', kwargs={'book_pk': book.pk})

    def get_chapter_create_absolute_url(self, book: Book) -> str:
        return self.get_absolute_url(self.get_chapter_create_path(book))

    def get_chapter_create_page(self, book: Book, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_chapter_create_absolute_url(book))
        return page

    def get_chapter_list_path(self, book: Book) -> str:
        return reverse('core:book_read', kwargs={'pk': book.pk}) + '?tab=chapters'

    def get_chapter_list_absolute_url(self, book: Book) -> str:
        return self.get_absolute_url(self.get_chapter_list_path(book))

    def get_chapter_list_page(self, book: Book, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_chapter_list_absolute_url(book))
        return page

    def get_chapter_update_path(self, chapter: Chapter, book: Book) -> str:
        return reverse('core:chapter_update', kwargs={'pk': chapter.pk, 'book_pk': book.pk})

    def get_chapter_update_page(self, chapter: Chapter, book: Book, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_chapter_update_absolute_url(chapter, book))
        return page

    def get_chapter_update_absolute_url(self, chapter: Chapter, book: Book) -> str:
        return self.get_absolute_url(self.get_chapter_update_path(chapter, book))

    def get_book_read_absolute_url(self, book: Book) -> str:
        return self.get_absolute_url(book.get_absolute_url())

    def assert_is_forbidden_page(self, page: Page):
        self.assertEqual(page.inner_text('body'), self.msg_forbidden)


class ChapterListStoryMixin(ChapterStoryPagesMixin):

    # alerts
    chapterlessness_alert_msg = _("It looks like you've not yet created any chapters for this book.")

    # ctas
    add_chapter_cta = _('Add chapter')
    chapter_delete_cta = _('Delete')
    chapter_delete_cancel_cta = _('Go back')
    chapter_delete_confirm_cta = _('Confirm delete')
    chapter_update_cta = _('Update')

    # messages
    delete_modal_message = _("You're about to delete the chapter")

    def assert_chapterlessness_alert_is_present(self, page: Page):
        alert_elt = page.query_selector(f'.alert:has-text("{self.chapterlessness_alert_msg}")')
        self.assertIsNotNone(alert_elt)

    def assert_chapterlessness_alert_is_not_present(self, page: Page):
        alert_elt = page.query_selector(f'.alert:has-text("{self.chapterlessness_alert_msg}")')
        self.assertIsNone(alert_elt)

    def assert_add_chapter_button_present_and_working(self, page: Page, book: Book):
        add_chapter_button = page.query_selector(f'text="{self.add_chapter_cta}"')
        self.assertIsNotNone(add_chapter_button)

        add_chapter_button.click()
        expected_redirect_url = self.get_chapter_create_absolute_url(book)
        self.assertEqual(page.url, expected_redirect_url)
        self.reset_page(page, book)

    def assert_add_chapter_button_not_present(self, page: Page):
        add_chapter_button = page.query_selector(f'text="{self.add_chapter_cta}"')
        self.assertIsNone(add_chapter_button)

    def assert_page_contains_chapters_without_action_buttons(self, page: Page, book: Book, chapters: List[Chapter]):

        for chapter in chapters:
            chapter_num = chapter.number
            with self.subTest(chapter_number=chapter_num):
                toggler_text = _('Chapter') + f' {chapter.number}: {chapter.title}'
                page.click(f'text={toggler_text}')
                page.wait_for_selector(f'#id_collapse-{chapter_num}.collapse.show')
                content_elt = page.query_selector(f':below(:text("{toggler_text}"))')

                self.assertIsNotNone(content_elt)
                content = content_elt.inner_text()
                self.assertIn(chapter.url, content)
                self.assertIn(chapter.description, content)

                # delete and update button are absent
                delete_button = content_elt.query_selector(f'text="{self.chapter_delete_cta}"')
                update_button = content_elt.query_selector(f'text="{self.chapter_update_cta}"')
                self.assertIsNone(delete_button)
                self.assertIsNone(update_button)

    def assert_page_contains_chapters_with_action_buttons(self, page: Page, book: Book, chapters: List[Chapter]):

        for chapter in chapters:
            chapter_num = chapter.number
            with self.subTest(chapter_number=chapter_num):
                # cache initial url
                init_url = page.url

                toggler_text = _('Chapter') + f' {chapter.number}: {chapter.title}'
                page.click(f'text={toggler_text}')
                page.wait_for_selector(f'#id_collapse-{chapter_num}.collapse.show')
                content_elt = page.query_selector(f':below(:text("{toggler_text}"))')

                self.assertIsNotNone(content_elt)
                content = content_elt.inner_text()
                self.assertIn(chapter.url, content)

                # delete and update button are present
                delete_button = content_elt.query_selector(f'text="{self.chapter_delete_cta}"')
                update_button = content_elt.query_selector(f'text="{self.chapter_update_cta}"')
                self.assertIsNotNone(delete_button)
                self.assertIsNotNone(update_button)

                # delete button pops up the deletion dialog
                delete_button.click()
                delete_modal_selector = f'#id_delete_modal-{chapter_num}.show'
                page.wait_for_selector(delete_modal_selector)
                self.assertEqual(page.url, init_url)

                delete_modal = page.query_selector(delete_modal_selector)
                # deletion dialog contains
                # - an appropriate deletion message
                # - the appropriate cancel and confirm actions
                self.assertIn(self.delete_modal_message, delete_modal.inner_text())
                delete_cancel_button = delete_modal.query_selector(f'text="{self.chapter_delete_cancel_cta}"')
                delete_cancel_button.click()

                delete_modal = page.query_selector(delete_modal_selector)
                self.assertIsNone(delete_modal)
                self.assertEqual(page.url, init_url)

                # update button redirects to the appropriate chapter_update page
                update_button.click()
                expected_redirect_url = self.get_chapter_update_absolute_url(chapter, book)
                self.assertEqual(page.url, expected_redirect_url)

                # reset page because last assertion left it at some other page
                self.reset_page(page, book)

    def reset_page(self, page: Page, book: Book):
        page.goto(self.get_chapter_list_absolute_url(book))


class ChapterCreateStoryMixin(ChapterStoryPagesMixin):
    pass


class ChapterUpdateStoryMixin(ChapterStoryPagesMixin):
    pass


class ChapterDeleteStoryMixin(ChapterStoryPagesMixin):

    # alerts
    alert_delete_successful = _("You've just deleted the chapter")

    # ctas
    chapter_delete_cta = _('Delete')
    chapter_delete_confirm_cta = _('Confirm delete')

    def assert_can_delete_chapter_from_page(self, chapter: Chapter, page: Page):

        # chapter toggler is in the list
        chapter_toggler_text = _('Chapter') + f' {chapter.number}: {chapter.title}'
        chapter_toggler = page.query_selector(f'text={chapter_toggler_text}')
        self.assertIsNotNone(chapter_toggler)

        # toggle the chapter card open
        chapter_toggler.click()
        page.wait_for_selector(f'#id_collapse-{chapter.number}.collapse.show')
        content_elt = page.query_selector(f':below(:text("{chapter_toggler_text}"))')

        # click on the delete button in the opened card
        delete_button = content_elt.query_selector(f'text="{self.chapter_delete_cta}"')
        delete_button.click()

        # confirm the delete action in the modal that pops up
        delete_modal_selector = f'#id_delete_modal-{chapter.number}.show'
        page.wait_for_selector(delete_modal_selector)
        delete_modal = page.query_selector(delete_modal_selector)
        delete_confirm_button = delete_modal.query_selector(f'text="{self.chapter_delete_confirm_cta}"')
        delete_confirm_button.click()

        # chapter is no longer in the list
        chapter_toggler = page.query_selector(f'text={chapter_toggler_text}')
        self.assertIsNone(chapter_toggler)

    def assert_page_was_redirected_to_chapter_list_page(self, page: Page, book: Book):
        # expected_redirect_url = self.get_chapter_list_absolute_url(book)
        # self.assertEqual(page.url, expected_redirect_url)
        self.assertIn(page.url, [self.get_chapter_list_absolute_url(book), self.get_book_read_absolute_url(book)])

    def assert_delete_success_alert_is_seen_on_page(self, page: Page, chapter: Chapter):
        alert_elt = page.query_selector(f'text={self.alert_delete_successful}')
        self.assertIsNotNone(alert_elt)

        alert_elt_text = alert_elt.inner_text()
        self.assertIn(str(chapter.number), alert_elt_text)
        self.assertIn(chapter.title, alert_elt_text)
