import unittest
from typing import Callable

from playwright.sync_api import sync_playwright
from playwright.sync_api._generated import Browser, BrowserContext, Page, Playwright

from django.contrib.staticfiles.testing import StaticLiveServerTestCase as DjangoStaticLiveServerTestCase
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from model_bakery import baker

from learnity import utils


@unittest.skipIf(not utils.is_running_functional_tests(), 'not running functional tests')
class FunctionalTestCaseMixin:
    pass


class PlaywrightStoryMixin:
    def iter_browser_types(self, playwright: Playwright):
        yield playwright.chromium
        # yield playwright.firefox
        # yield playwright.webkit

    def test_story(self):
        with sync_playwright() as playwright:
            for browser_type in self.iter_browser_types(playwright):
                browser = browser_type.launch(headless=False)
                with self.subTest(browser_name=browser_type.name):
                    self.run_story(browser)
                for context in browser.contexts:
                    context.close()
                browser.close()

    def run_story(self, browser: Browser):
        raise NotImplementedError


class StoryMixin(PlaywrightStoryMixin, FunctionalTestCaseMixin):
    pass


class UserStoryMixin(StoryMixin):

    login_cta = _('Log in')

    get_absolute_url: Callable[[str], str]
    assertNotEqual: Callable[[str, str], bool]

    def _fixture_setup(self):
        super()._fixture_setup()

        # user: Alice
        # accessor: self.owner, self.user
        # use case: owner of an object being acted upon in a view
        self._password = 'heyy, my name 15 A11ce!'
        self._username = 'alice@aol.com'
        self.user = baker.prepare_recipe('accounts.tests.activated_user', email=self._username)
        self.user.set_password(self._password)
        self.user.save()
        self.owner = self.user

        # user: ----
        # accessor: self.non_owner, self.alt_user
        # use case: user who isn't owner of an object being acted upon in a view
        self._alt_password = 'Ca11 me A1t User!'
        self._alt_username = 'altuser@aol.com'
        self.alt_user = baker.prepare_recipe('accounts.tests.activated_user', email=self._alt_username)
        self.alt_user.set_password(self._alt_password)
        self.alt_user.save()
        self.non_owner = self.alt_user

    def get_login_path(self) -> str:
        return reverse('login')

    def get_login_absolute_url(self) -> str:
        return self.get_absolute_url(self.get_login_path())

    def login_user(self, context: BrowserContext) -> Page:
        page = context.new_page()
        login_url = self.get_login_absolute_url()
        page.goto(login_url)
        page.fill('id=id_username', self._username)
        page.fill('id=id_password', self._password)
        page.click(f'text="{self.login_cta}"')
        self.assertNotEqual(page.url, login_url)
        return page

    def login_owner(self, context: BrowserContext) -> Page:
        return self.login_user(context)

    def login_alt_user(self, context: BrowserContext) -> Page:
        page = context.new_page()
        login_url = self.get_login_absolute_url()
        page.goto(login_url)
        page.fill('id=id_username', self._alt_username)
        page.fill('id=id_password', self._alt_password)
        page.click(f'text="{self.login_cta}"')
        self.assertNotEqual(page.url, login_url)
        return page

    def login_non_owner(self, context: BrowserContext) -> Page:
        return self.login_alt_user(context)


class LiveServerURLMixin:
    def get_absolute_url(self, path: str) -> str:
        return f'{self.live_server_url}{path}'


class StaticLiveServerTestCase(DjangoStaticLiveServerTestCase, LiveServerURLMixin):
    pass
