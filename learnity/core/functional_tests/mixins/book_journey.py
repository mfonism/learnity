from typing import Callable

from playwright.sync_api._generated import BrowserContext, Page

from django.urls import reverse
from django.utils.translation import gettext as _

from core.models import Book, BookJourney, Chapter, ChapterJourney


class BookJourneyStoryPagesMixin:

    get_absolute_url: Callable[[str], str]

    # messages
    msg_forbidden = _('403 Forbidden')

    def assert_is_forbiden_page(self, page: Page):
        self.assertEqual(page.inner_text('body'), self.msg_forbidden)

    def get_book_journey_read_path(self, book_journey: BookJourney) -> str:
        return reverse('core:book_journey_read', kwargs={'pk': book_journey.pk})

    def get_book_journey_read_absolute_url(self, book_journey: BookJourney) -> str:
        return self.get_absolute_url(self.get_book_journey_read_path(book_journey))

    def get_book_journey_read_page(self, book_journey: BookJourney, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_book_journey_read_absolute_url(book_journey))
        return page

    def get_chapter_journey_create_path(self, chapter: Chapter, book_journey: BookJourney) -> str:
        return reverse(
            'core:chapter_journey_create', kwargs={'chapter_pk': chapter.pk, 'book_journey_pk': book_journey.pk}
        )

    def get_chapter_journey_create_absolute_url(self, chapter: Chapter, book_journey: BookJourney) -> str:
        return self.get_absolute_url(self.get_chapter_journey_create_path(chapter, book_journey))

    def get_chapter_journey_read_path(self, chapter_journey: ChapterJourney) -> str:
        return reverse('core:chapter_journey_read', kwargs={'pk': chapter_journey.pk})

    def get_chapter_journey_read_absolute_url(self, chapter_journey: ChapterJourney) -> str:
        return self.get_absolute_url(self.get_chapter_journey_read_path(chapter_journey))

    def get_book_read_path(self, book: Book) -> str:
        return reverse('core:book_read', kwargs={'pk': book.pk})

    def get_book_read_absolute_url(self, book: Book) -> str:
        return self.get_absolute_url(self.get_book_read_path(book))


class BookJourneyReadStoryMixin(BookJourneyStoryPagesMixin):
    def assert_page_contains_personalized_titles(self, page: Page, title_head: str, book: Book):
        title_head_elt = page.query_selector(f'text={title_head}')
        self.assertIsNotNone(title_head_elt)
        # underlying book title is also present
        book_title_elt = page.query_selector(f'text="{book.title}"')
        self.assertIsNotNone(book_title_elt)
        # clicking on the title takes to appropriate book_read page
        book_title_elt.click()
        self.assertEqual(page.url, self.get_book_read_absolute_url(book))
        page.go_back()

    def assert_chapter_journey_card_is_absent_from_page(self, chapter: Chapter, page: Page):
        chapter_journey_card = page.query_selector(f'.card:has-text("{chapter.title}")')
        self.assertIsNone(chapter_journey_card)

    def assert_chapter_journey_card_is_uninitialized_on_page(self, chapter: Chapter, page: Page):
        # there is a card containing title of the chapter
        # and a cta to initialize a chapter journey
        init_url = page.url
        chapter_journey_card = page.query_selector(f'.card:has-text("{chapter.title}")')
        self.assertIsNotNone(chapter_journey_card)

        cta = _('Start chapter journey')
        create_button = chapter_journey_card.query_selector(f'text="{cta}"')
        self.assertIsNotNone(create_button)

        create_button.click()
        created_chapter_journey = ChapterJourney.objects.latest('created_at')
        expected_redirect_url = reverse('core:chapter_journey_read', kwargs={'pk': created_chapter_journey.pk})
        page.goto(init_url)

    def assert_chapter_journey_card_is_readonly_on_page(
        self, chapter: Chapter, chapter_journey: ChapterJourney, page: Page
    ):
        # there is a card containing title of the chapter
        # and a cta to read the associated chapter journey
        chapter_journey_card = page.query_selector(f'.card:has-text("{chapter.title}")')
        self.assertIsNotNone(chapter_journey_card)

        cta = _('Explore')
        read_notes_button = chapter_journey_card.query_selector(f'text="{cta}"')
        self.assertIsNotNone(read_notes_button)

        read_notes_button.click()
        self.assertEqual(page.url, self.get_chapter_journey_read_absolute_url(chapter_journey))
        page.go_back()
