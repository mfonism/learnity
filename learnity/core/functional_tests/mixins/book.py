from typing import Callable, List

from playwright.sync_api._generated import BrowserContext, Page

from django.template.defaultfilters import truncatechars
from django.urls import reverse
from django.utils.translation import gettext as _

from core.models import Book, BookJourney, Chapter


class BookStoryPagesMixin:

    get_absolute_url: Callable[[str], str]

    # messages
    msg_forbidden = _('403 Forbidden')

    def get_book_create_path(self) -> str:
        return reverse('core:book_create')

    def get_book_create_absolute_url(self) -> str:
        return self.get_absolute_url(self.get_book_create_path())

    def get_book_create_page(self, context: BrowserContext) -> str:
        page = context.new_page()
        page.goto(self.get_book_create_absolute_url())
        return page

    def get_book_read_path(self, book: Book) -> str:
        return reverse('core:book_read', kwargs={'pk': book.pk})

    def get_book_read_absolute_url(self, book: Book) -> str:
        return self.get_absolute_url(self.get_book_read_path(book))

    def get_book_read_page(self, book: Book, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_book_read_absolute_url(book))
        return page

    def get_book_update_path(self, book: Book) -> str:
        return reverse('core:book_update', kwargs={'pk': book.pk})

    def get_book_update_absolute_url(self, book: Book) -> str:
        return self.get_absolute_url(self.get_book_update_path(book))

    def get_book_update_page(self, book: Book, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_book_update_absolute_url(book))
        return page

    def get_book_list_path(self) -> str:
        return reverse('core:book_list')

    def get_book_list_absolute_url(self) -> str:
        return self.get_absolute_url(self.get_book_list_path())

    def get_book_list_page(self, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_book_list_absolute_url())
        return page

    def get_own_book_list_path(self) -> str:
        return reverse('core:own_book_list')

    def get_own_book_list_absolute_url(self) -> str:
        return self.get_absolute_url(self.get_own_book_list_path())

    def get_own_book_list_page(self, context: BrowserContext) -> Page:
        page = context.new_page()
        page.goto(self.get_own_book_list_absolute_url())
        return page

    def get_book_journey_create_path(self, book: Book) -> str:
        return reverse('core:book_journey_create', kwargs={'book_pk': book.pk})

    def assert_is_forbidden_page(self, page: Page):
        self.assertEqual(page.inner_text('body'), self.msg_forbidden)


class BookCreateStoryMixin(BookStoryPagesMixin):
    pass


class BookReadStoryMixin(BookStoryPagesMixin):

    # tab ctas
    chapters_tab_cta = _('Chapters')
    summary_tab_cta = _('Summary')

    # book action ctas
    book_add_chapter_cta = _('Add chapter')
    book_delete_cta = _('Delete')
    book_publish_cta = _('Publish')
    book_update_cta = _('Update')

    # book action cancel and confirm ctas
    book_delete_cancel_cta = _('Go back')
    book_delete_confirm_cta = _('Confirm delete')
    book_publish_cancel_cta = _('Go back')
    book_publish_confirm_cta = _('Confirm publish')

    # book-journey action ctas
    book_journey_start_cta = _('Start book journey')
    book_journey_read_cta = _('Explore')

    def assert_page_contains_summary_and_chapter_panes(self, page: Page, book: Book):
        # cache page url for future checks
        url = page.url

        # summary pane is active/shown
        # chapters pane is inactive/not-shown
        self.assertIsNotNone(page.query_selector('#id_summary.tab-pane.active.show'))
        self.assertIsNone(page.query_selector('#id_chapters.tab-pane.active.show'))

        # clicking on the chapters tab toggles the chapters pane into view
        page.click(f'text="{self.chapters_tab_cta}"')
        page.wait_for_selector('#id_chapters.tab-pane.active.show')
        self.assertEqual(page.url, url)
        self.assertIsNone(page.query_selector('#id_summary.tab-pane.active.show'))
        self.assertIsNotNone(page.query_selector('#id_chapters.tab-pane.active.show'))

        # clicking on the summary tab toggles the summary pane into view
        page.click(f'text="{self.summary_tab_cta}"')
        page.wait_for_selector('#id_summary.tab-pane.active.show')
        self.assertEqual(page.url, url)
        self.assertIsNotNone(page.query_selector('#id_summary.tab-pane.active.show'))
        self.assertIsNone(page.query_selector('#id_chapters.tab-pane.active.show'))

    def assert_page_summary_pane_contains_book_details(self, page: Page, book: Book):
        """
        Assert that:
        * the summary pane contains the following details of the book:
          - title
          - subtitle (only if book has subtitle)
          - abstract
          - url
          - description
        """
        # book title is present
        self.assertIsNotNone(page.query_selector(f'text="{book.title}"'))
        # book subtitle is present if book has subtitle
        # otherwise it is absent
        subtitle_elt = page.query_selector(f'text="{book.subtitle}"')
        if book.subtitle is not None:
            self.assertIsNotNone(subtitle_elt)
        else:
            self.assertIsNone(subtitle_elt)
        # book abstract is present
        self.assertIsNotNone(page.query_selector(f'text="{book.abstract}"'))
        # book URL is present
        anchor = page.query_selector(f'//a[@href="{book.url}"]')
        self.assertEqual(anchor.inner_text(), book.url)
        # book description is present
        self.assertIsNotNone(page.query_selector(f'text="{book.description}"'))

    def assert_page_chapters_pane_contains_book_chapters(self, page: Page, book: Book, chapters: List[Chapter]):
        """
        Assert that all the chapters are listed in the chapter pane
        of the book_read page for input book.
        Assert that each chapter's number, title, url and description is present.
        """
        page.click(f'text="{self.chapters_tab_cta}"')
        page.wait_for_selector('#id_chapters.tab-pane.active.show')

        for chapter in chapters:
            chapter_num = chapter.number
            with self.subTest(chapter_number=chapter_num):
                toggler_text = _('Chapter') + f' {chapter.number}: {chapter.title}'
                page.click(f'text={toggler_text}')
                page.wait_for_selector(f'#id_collapse-{chapter_num}.collapse.show')
                content_elt = page.query_selector(f':below(:text("{toggler_text}"))')

                self.assertIsNotNone(content_elt)
                content = content_elt.inner_text()
                self.assertIn(chapter.url, content)
                self.assertIn(chapter.description, content)

    def assert_update_button_present_and_working(self, page: Page, book: Book):
        # clicking on the update button leads to the book_update page

        # cache page url to return to it at the end of everything
        url = page.url

        page.click(f'text="{self.book_update_cta}"')
        expected_url = self.get_book_update_absolute_url(book)
        self.assertEqual(page.url, expected_url)
        page.goto(url)

    def assert_update_button_not_present(self, page: Page):
        update_button = page.query_selector(f'text="{self.book_update_cta}"')
        self.assertIsNone(update_button)

    def assert_delete_button_present_and_working(self, page: Page, book: Book):
        # cache page url for future checks
        url = page.url

        # clicking on the delete button brings up the deletion modal
        page.click(f'text="{self.book_delete_cta}"')
        page.wait_for_selector('#id_delete_modal.show')
        self.assertEqual(page.url, url)

        # deletion modal contains the correct deletion message
        deletion_modal = page.query_selector('#id_delete_modal')
        msg = _("You're about to delete the book") + f' {book.title}'
        self.assertIsNotNone(deletion_modal.query_selector(f'text={msg}'))
        # and the correct call to action buttons
        self.assertIsNotNone(deletion_modal.query_selector(f'text="{self.book_delete_cancel_cta}"'))
        self.assertIsNotNone(deletion_modal.query_selector(f'text="{self.book_delete_confirm_cta}"'))

        # deletion can be canceled
        page.click(f'text="{self.book_delete_cancel_cta}"')
        page.wait_for_selector('#id_delete_modal:not(.show)')
        self.assertEqual(page.url, url)

    def assert_delete_button_not_present(self, page: Page):
        delete_button = page.query_selector(f'text="{self.book_delete_cta}"')
        self.assertIsNone(delete_button)

    def assert_publish_button_present_and_working(self, page: Page, book: Book):
        # cache page url for future checks
        url = page.url

        # clicking on the publish button brings up the publish modal
        page.click(f'text="{self.book_publish_cta}"')
        page.wait_for_selector('#id_publish_modal.show')
        self.assertEqual(page.url, url)

        # publish modal contains the correct publish message
        publish_modal = page.query_selector('#id_publish_modal')
        msg = _("You're about to publish the book") + f' {book.title}'
        self.assertIsNotNone(publish_modal.query_selector(f'text={msg}'))
        # and the correct call to action buttons
        self.assertIsNotNone(publish_modal.query_selector(f'text="{self.book_publish_cancel_cta}"'))
        self.assertIsNotNone(publish_modal.query_selector(f'text="{self.book_publish_confirm_cta}"'))

        # publish action can be canceled
        page.click(f'text="{self.book_publish_cancel_cta}"')
        page.wait_for_selector('#id_publish_modal:not(.show)')
        self.assertEqual(page.url, url)

    def assert_publish_button_not_present(self, page: Page):
        publish_button = page.query_selector(f'text="{self.book_publish_cta}"')
        self.assertIsNone(publish_button)

    def assert_publication_details_present(self, page: Page, publisher_name: str):
        publication_time_lead = _('Added:')
        publication_elt = page.query_selector(f'div:has-text("{publication_time_lead}")')

        self.assertIsNotNone(publication_elt)
        self.assertIn(f'by {publisher_name}', publication_elt.inner_text())

    def assert_publication_details_not_present(self, page: Page):
        publication_time_lead = _('Added:')
        self.assertIsNone(page.query_selector(f'div:has-text("{publication_time_lead}")'))

    def assert_add_chapter_button_present_and_working(self, page: Page, book: Book):
        page.click(f'text="{self.chapters_tab_cta}"')
        page.wait_for_selector('#id_chapters.tab-pane.active.show')

        page.click(f'text="{self.book_add_chapter_cta}"')
        expected_url = self.get_absolute_url(reverse('core:chapter_create', kwargs={'book_pk': book.pk}))
        self.assertEqual(page.url, expected_url)

    def assert_add_chapter_button_not_present(self, page: Page):
        page.click(f'text="{self.chapters_tab_cta}"')
        page.wait_for_selector('#id_chapters.tab-pane.active.show')

        add_chapter_button = page.query_selector(f'text="{self.book_add_chapter_cta}"')
        self.assertIsNone(add_chapter_button)

    def assert_start_book_journey_button_not_present(self, page: Page):
        start_book_journey_button = page.query_selector(f'text="{self.book_journey_start_cta}"')
        self.assertIsNone(start_book_journey_button)

    def assert_your_book_journey_button_present(self, book_journey: BookJourney, page: Page):
        your_book_journey_button = page.query_selector(f'text="{self.book_journey_read_cta}"')
        self.assertIsNotNone(your_book_journey_button)

        your_book_journey_button.click()
        book_journey_read_path = reverse('core:book_journey_read', kwargs={'pk': book_journey.pk})
        self.assertEqual(page.url, self.get_absolute_url(book_journey_read_path))
        page.go_back()

    def assert_start_book_journey_button_present(self, page: Page, book: Book, is_user_anon: bool):
        init_url = page.url

        start_book_journey_button = page.query_selector(f'text="{self.book_journey_start_cta}"')
        self.assertIsNotNone(start_book_journey_button)
        start_book_journey_button.click()

        if is_user_anon:
            # anonymous user is redirected to the login page
            # to be returned to book_read page on successful login
            expected_redirect_url = self.get_login_absolute_url() + '?next=' + self.get_book_journey_create_path(book)
            self.assertEqual(page.url, expected_redirect_url)
        else:
            # auth user is redirected to read page for created book_journey
            created_book_journey = BookJourney.objects.latest('created_at')
            book_journey_read_path = reverse('core:book_journey_read', kwargs={'pk': created_book_journey.pk})
            self.assertEqual(page.url, self.get_absolute_url(book_journey_read_path))

            page.goto(init_url)
            self.assert_start_book_journey_button_not_present(page)
            self.assert_your_book_journey_button_present(created_book_journey, page)

        page.goto(init_url)


class BookUpdateStoryMixin(BookStoryPagesMixin):
    pass


class BookDeleteStoryMixin(BookStoryPagesMixin):
    pass


class BookPublishStoryMixin(BookStoryPagesMixin):
    pass


class BookListStoryMixin(BookStoryPagesMixin):
    def assert_page_contains_books(self, page: Page, books: List[Book]):
        init_url = page.url
        for count, book in enumerate(books):
            with self.subTest(count=count):
                title_head = truncatechars(book.title, 40)
                book_card = page.query_selector(f'.card:has-text("{title_head}")')
                self.assertIsNotNone(book_card)

                card_text = book_card.inner_text()
                self.assertIn(book.abstract, card_text)
                if book.is_published():
                    self.assertIn(_('Added:'), card_text)

                page.click(f'text="{title_head}"')
                expected_redirect_url = self.get_book_read_absolute_url(book)
                self.assertEqual(page.url, expected_redirect_url)
                page.goto(init_url)

    def assert_page_does_not_contain_books(self, page: Page, books: List[Book]):
        for count, book in enumerate(books):
            with self.subTest(count=count):
                title_head = truncatechars(book.title, 40)
                book_card = page.query_selector(f'.card:has-text("{title_head}")')
                self.assertIsNone(book_card)
