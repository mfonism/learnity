from contextlib import closing
from typing import List

from playwright.sync_api._generated import Browser

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.chapter import ChapterListStoryMixin
from core.models import Book, Chapter


class TestChapterList(UserStoryMixin, StaticLiveServerTestCase, ChapterListStoryMixin):
    def run_story(self, browser: Browser):
        # Alice has a yet unpublished book
        book = baker.make_recipe('core.tests.book', created_by=self.owner)

        # run appropriate story for book in inital state
        self.run_unpublished_chapterless_book_story(book, browser)

        # add chapters to unpublished book and run appropriate story
        num_chapters = 3
        numbers = list(range(num_chapters, 0, -1))
        chapters = baker.make_recipe('core.tests.chapter', number=numbers.pop, book=book, _quantity=num_chapters)
        self.run_unpublished_chaptered_book_story(book, chapters, browser)

        # publish book and run appropriate story
        book.publish()
        self.run_published_book_story(book, chapters, browser)

    def run_unpublished_chapterless_book_story(self, book: Book, browser: Browser):
        self.assertEqual(book.is_published(), False)
        self.assertEqual(book.has_chapters(), False)

        # anonymous user is forbidden from GETing chapter_list page
        with closing(browser.new_context()) as context:
            page = self.get_chapter_list_page(book, context)
            self.assert_is_forbidden_page(page)

        # non-owner is forbidden from GETing chapter_list page
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_chapter_list_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can GET chapter_list page
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_chapter_list_page(book, context)
            self.assert_chapterlessness_alert_is_present(page)
            self.assert_add_chapter_button_present_and_working(page, book)

    def run_unpublished_chaptered_book_story(self, book: Book, chapters: List[Chapter], browser: Browser):
        self.assertEqual(book.is_published(), False)
        self.assertEqual(book.has_chapters(), True)

        # anonymous user is forbidden from GETing chapter_list page
        with closing(browser.new_context()) as context:
            page = self.get_chapter_list_page(book, context)
            self.assert_is_forbidden_page(page)

        # non-owner is forbidden from GETing chapter_list page
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_chapter_list_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can GET chapter_list page
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_chapter_list_page(book, context)
            self.assert_chapterlessness_alert_is_not_present(page)
            self.assert_add_chapter_button_present_and_working(page, book)
            self.assert_page_contains_chapters_with_action_buttons(page, book, chapters)

    def run_published_book_story(self, book: Book, chapters: List[Chapter], browser: Browser):
        self.assertEqual(book.is_published(), True)
        self.assertEqual(book.has_chapters(), True)

        # anonymous user can GET chapter_list page
        with closing(browser.new_context()) as context:
            page = self.get_chapter_list_page(book, context)
            self.assert_chapterlessness_alert_is_not_present(page)
            self.assert_add_chapter_button_not_present(page)
            self.assert_page_contains_chapters_without_action_buttons(page, book, chapters)

        # non-owner can GET chapter_list page
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_chapter_list_page(book, context)
            self.assert_chapterlessness_alert_is_not_present(page)
            self.assert_add_chapter_button_not_present(page)
            self.assert_page_contains_chapters_without_action_buttons(page, book, chapters)

        # owner can GET chapter_list page
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_chapter_list_page(book, context)
            self.assert_chapterlessness_alert_is_not_present(page)
            self.assert_add_chapter_button_present_and_working(page, book)
            self.assert_page_contains_chapters_with_action_buttons(page, book, chapters)
