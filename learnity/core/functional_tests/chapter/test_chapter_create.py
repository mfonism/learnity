from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.chapter import ChapterCreateStoryMixin
from core.models import Chapter


class TestChapterCreate(UserStoryMixin, StaticLiveServerTestCase, ChapterCreateStoryMixin):

    # chapter_create data
    chapter_number = 1
    chapter_title = 'Hello World'

    # alerts
    alert_create_successful = _("You've just created a chapter")

    # ctas
    chapter_create_cta = _('Create chapter')

    def run_story(self, browser: Browser):
        book = baker.make_recipe('core.tests.book', created_by=self.owner)
        baker.make_recipe('core.tests.chapter', book=book, _quantity=2)

        # anonymous user is redirected to login
        with closing(browser.new_context()) as context:
            page = self.get_chapter_create_page(book, context)
            expected_redirect_url = self.get_login_absolute_url() + '?next=' + self.get_chapter_create_path(book)
            self.assertEqual(page.url, expected_redirect_url)

        # non-owner is forbidden from creating chapter on someone else's book
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_chapter_create_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can create chapter on their book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_chapter_create_page(book, context)

            # page contains information about book
            book_read_anchor_elt = page.query_selector(f'a:has-text("{book.title}")')
            self.assertIsNotNone(book_read_anchor_elt)
            book_read_anchor_elt.click()
            self.assertEqual(page.url, self.get_book_read_absolute_url(book))
            page.go_back()

            # page contains chapter_create form with the correct fields
            number_field = page.query_selector('id=id_number')
            title_field = page.query_selector('id=id_title')
            url_field = page.query_selector('id=id_url')
            description_field = page.query_selector('id=id_description')
            self.assertIsNotNone(number_field)
            self.assertIsNotNone(title_field)
            self.assertIsNotNone(url_field)
            self.assertIsNotNone(description_field)

            # owner fills out the fields and submits data
            number_field.fill(str(self.chapter_number))
            title_field.fill(self.chapter_title)
            page.click(f'text="{self.chapter_create_cta}"')

            # owner is redirected to chapter_list page
            # for the underlying book
            created_chapter = Chapter.objects.filter(book__created_by_id=self.owner.pk).latest('created_at')
            expected_redirect_url = self.get_chapter_list_absolute_url(book)
            self.assertEqual(page.url, expected_redirect_url)

            # owner sees appropriate alert message
            alert_elt = page.query_selector(f'text={self.alert_create_successful}')
            self.assertIsNotNone(alert_elt)
            alert_elt_text = alert_elt.inner_text()
            self.assertIn(str(created_chapter.number), alert_elt_text)
            self.assertIn(created_chapter.title, alert_elt_text)
