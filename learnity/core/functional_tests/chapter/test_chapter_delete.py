from contextlib import closing

from playwright.sync_api._generated import Browser

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.chapter import ChapterDeleteStoryMixin


class TestChapterDelete(UserStoryMixin, StaticLiveServerTestCase, ChapterDeleteStoryMixin):
    def run_story(self, browser: Browser):

        book = baker.make_recipe('core.tests.book', created_by=self.owner)

        num_chapters = 5
        numbers = list(range(num_chapters, 0, -1))
        chapters = baker.make_recipe('core.tests.chapter', number=numbers.pop, book=book, _quantity=num_chapters)

        # anonymous user isn't shown the chapter delete button
        # and cannot therefore initiate chapter_delete

        # non-owner isn't shown the chapter delete button
        # and cannot therefore initiate chapter_delete

        # owner can initiate chapter_delete on a chapter
        # on their own book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_chapter_list_page(book, context)

            # delete chapter at the top of the list
            chapter_1 = chapters[0]
            self.assert_can_delete_chapter_from_page(chapter_1, page)
            self.assert_page_was_redirected_to_chapter_list_page(page, book)
            self.assert_delete_success_alert_is_seen_on_page(page, chapter_1)

            # delete chapter at the bottom of the list
            chapter_5 = chapters[4]
            self.assert_can_delete_chapter_from_page(chapter_5, page)
            self.assert_page_was_redirected_to_chapter_list_page(page, book)
            self.assert_delete_success_alert_is_seen_on_page(page, chapter_5)

            # delete chapter in the middle of the list
            chapter_3 = chapters[2]
            self.assert_can_delete_chapter_from_page(chapter_3, page)
            self.assert_page_was_redirected_to_chapter_list_page(page, book)
            self.assert_delete_success_alert_is_seen_on_page(page, chapter_3)
