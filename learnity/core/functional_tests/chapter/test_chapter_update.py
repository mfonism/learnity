from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.chapter import ChapterUpdateStoryMixin


class TestChapterUpdate(UserStoryMixin, StaticLiveServerTestCase, ChapterUpdateStoryMixin):

    # alerts
    alert_update_successful = _("You've just updated the chapter")

    # ctas
    chapter_update_cta = _('Update chapter')

    # payload
    updated_title = 'Updated title'
    updated_url = 'https://updated.title.com'

    def run_story(self, browser: Browser):
        book = baker.make_recipe('core.tests.book', created_by=self.owner)
        chapter = baker.make_recipe('core.tests.chapter', book=book)

        # anonymous user is redirected to login
        with closing(browser.new_context()) as context:
            page = self.get_chapter_update_page(chapter, book, context)
            expected_redirect_url = (
                self.get_login_absolute_url() + '?next=' + self.get_chapter_update_path(chapter, book)
            )
            self.assertEqual(page.url, expected_redirect_url)

        # non-owner is forbidden from updating chapter on someone else's book
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_chapter_update_page(chapter, book, context)
            self.assert_is_forbidden_page(page)

        # owner can update chapter on their book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_chapter_update_page(chapter, book, context)

            # page contains information about the book
            book_read_anchor_elt = page.query_selector(f'a:has-text("{book.title}")')
            self.assertIsNotNone(book_read_anchor_elt)
            book_read_anchor_elt.click()
            self.assertEqual(page.url, self.get_book_read_absolute_url(book))
            page.go_back()

            # page contains chapter_update form with the correct fields
            number_field = page.query_selector('id=id_number')
            title_field = page.query_selector('id=id_title')
            url_field = page.query_selector('id=id_url')
            description_field = page.query_selector('id=id_description')
            self.assertIsNotNone(number_field)
            self.assertIsNotNone(title_field)
            self.assertIsNotNone(url_field)
            self.assertIsNotNone(description_field)

            # owner updates some data and submits
            title_field.fill(self.updated_title)
            url_field.fill(self.updated_url)
            page.click(f'text="{self.chapter_update_cta}"')
            chapter.refresh_from_db()

            # owner is redirected to chapter_list page
            # for the underlying book
            expected_redirect_url = self.get_chapter_list_absolute_url(book)
            self.assertEqual(page.url, expected_redirect_url)

            # owner sees appropriate alert message
            alert_elt = page.query_selector(f'text={self.alert_update_successful}')
            self.assertIsNotNone(alert_elt)
            alert_elt_text = alert_elt.inner_text()
            self.assertIn(str(chapter.number), alert_elt_text)
            self.assertIn(chapter.title, alert_elt_text)
