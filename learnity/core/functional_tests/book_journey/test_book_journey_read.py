from contextlib import closing
from typing import List

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book_journey import BookJourneyReadStoryMixin
from core.models import Book, BookJourney, Chapter, ChapterJourney


class TestBookJourneyRead(UserStoryMixin, StaticLiveServerTestCase, BookJourneyReadStoryMixin):
    def make_n_chapters(self, n: int, book: Book):
        numbers = list(range(n, 0, -1))
        return baker.make_recipe('core.tests.chapter', number=numbers.pop, book=book, _quantity=n)

    def run_story(self, browser: Browser):
        # owner has a book-journey on a published book
        book = baker.make_recipe('core.tests.published_book', created_by=self.owner)
        chapters = self.make_n_chapters(3, book)
        book_journey = baker.make_recipe('core.tests.book_journey', created_by=self.owner, book=book)

        self.run_chapterless_journey_story(book, book_journey, browser)

        # at the end of the previous story the book-journey has
        # chapter-journeys for all chapters in the book
        # add some more (non-journeyed) chapters to the book
        journeyed_chapters = chapters
        non_journeyed_chapters = self.make_n_chapters(2, book)
        chapter_journeys = [book_journey.get_chapter_journey_for_chapter(chapter) for chapter in chapters]
        self.run_chaptered_journey_story(
            book,
            book_journey,
            chapter_journeys,
            journeyed_chapters,
            non_journeyed_chapters,
            browser,
        )

    def run_chapterless_journey_story(self, book: Book, book_journey: BookJourney, browser: Browser):
        # anonymous user can read book-journey
        # but won't see anything about chapters or chapter-journeys here
        with closing(browser.new_context()) as context:
            page = self.get_book_journey_read_page(book_journey, context)

            expected_title_head = _('The book journey of') + ' ' + self.owner.name
            self.assert_page_contains_personalized_titles(page, expected_title_head, book)
            for chapter in book.get_chapters():
                with self.subTest(chapter_number=chapter.number):
                    self.assert_chapter_journey_card_is_absent_from_page(chapter, page)

        # non-owner can read book-journey
        # but won't see anything about chapters or chapter-journeys here
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_journey_read_page(book_journey, context)

            expected_title_head = _('The book journey of') + ' ' + self.owner.name
            self.assert_page_contains_personalized_titles(page, expected_title_head, book)
            for chapter in book.get_chapters():
                with self.subTest(chapter_number=chapter.number):
                    self.assert_chapter_journey_card_is_absent_from_page(chapter, page)

        # owner can read book-journey (and it is personalized)
        # and will see uninitialized chapter journey cards
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_journey_read_page(book_journey, context)

            # expected title head is present
            expected_title_head = _('Your book journey')
            self.assert_page_contains_personalized_titles(page, expected_title_head, book)
            for chapter in book.get_chapters():
                with self.subTest(chapter_number=chapter.number):
                    self.assert_chapter_journey_card_is_uninitialized_on_page(chapter, page)

    def run_chaptered_journey_story(
        self,
        book: Book,
        book_journey: BookJourney,
        chapter_journeys: List[ChapterJourney],
        journeyed_chapters: List[Chapter],
        non_journeyed_chapters: List[Chapter],
        browser: Browser,
    ):
        # anonymous user sees read only chapter journeys for journeyed chapters
        # and nothing for the rest
        with closing(browser.new_context()) as context:
            page = self.get_book_journey_read_page(book_journey, context)
            for chapter, chapter_journey in zip(journeyed_chapters, chapter_journeys):
                with self.subTest(chapter_pk=chapter.pk):
                    self.assertEqual(chapter_journey.chapter, chapter)
                    self.assert_chapter_journey_card_is_readonly_on_page(chapter, chapter_journey, page)

            for chapter in non_journeyed_chapters:
                with self.subTest(chapter_pk=chapter.pk):
                    self.assert_chapter_journey_card_is_absent_from_page(chapter, page)

        # non-owner sees readonly chapter-journeys for journeyed chapters
        # and nothing for the rest
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_journey_read_page(book_journey, context)
            for chapter, chapter_journey in zip(journeyed_chapters, chapter_journeys):
                with self.subTest(chapter_pk=chapter.pk):
                    self.assertEqual(chapter_journey.chapter, chapter)
                    self.assert_chapter_journey_card_is_readonly_on_page(chapter, chapter_journey, page)

            for chapter in non_journeyed_chapters:
                with self.subTest(chapter_pk=chapter.pk):
                    self.assert_chapter_journey_card_is_absent_from_page(chapter, page)

        # FOR NOW
        # owner sees readonly chapter-journeys for journeyed chapters
        # and sees the rest as uninitialized cards
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_journey_read_page(book_journey, context)
            for chapter, chapter_journey in zip(journeyed_chapters, chapter_journeys):
                with self.subTest(chapter_pk=chapter.pk):
                    self.assertEqual(chapter_journey.chapter, chapter)
                    self.assert_chapter_journey_card_is_readonly_on_page(chapter, chapter_journey, page)

            for chapter in non_journeyed_chapters:
                with self.subTest(chapter_pk=chapter.pk):
                    self.assert_chapter_journey_card_is_uninitialized_on_page(chapter, page)
