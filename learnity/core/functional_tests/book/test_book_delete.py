from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookDeleteStoryMixin
from core.models import Book


class TestBookDelete(UserStoryMixin, StaticLiveServerTestCase, BookDeleteStoryMixin):

    # alerts
    alert_delete_successful = _("You've just deleted the book")

    # ctas
    book_delete_cta = _('Delete')
    book_delete_confirm_cta = _('Confirm delete')

    def run_story(self, browser: Browser):
        # run story for unpublished book
        book = baker.make_recipe('core.tests.book', created_by=self.owner)
        self.run_unpublished_book_story(browser, book)

        # run story for published book
        published_book = baker.make_recipe('core.tests.published_book', created_by=self.owner)
        self.run_published_book_story(browser, published_book)

    def run_unpublished_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), False)

        # anonymous user is forbidden from GETing book_read page for
        # unpublished book and therefore cannot initiate book_delete

        # non-owner is forbidden from GETing book_read page for
        # unpublished book and therefore cannot initiate book_delete

        # owner can initiate and follow through with book_delete
        # for unpublished book from the book_read page
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_read_page(book, context)
            page.click(f'text="{self.book_delete_cta}"')
            page.wait_for_selector(f'text="{self.book_delete_confirm_cta}"')
            page.click(f'text="{self.book_delete_confirm_cta}"')

            success_url = self.get_own_book_list_absolute_url()
            self.assertEqual(page.url, success_url)

            # owner sees appropriate success alert on the redirected (destination) page
            alert_elt = page.query_selector(f'text={self.alert_delete_successful}')
            self.assertIsNotNone(alert_elt)
            self.assertIn(book.title, alert_elt.inner_text())

    def run_published_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), True)

        # anonymous user is forbidden from GETing book_read page for
        # published book and therefore cannot initiate book_delete

        # non-owner is forbidden from GETing book_read page for
        # published book and therefore cannot initiate book_delete

        # owner can initiate and follow through with book_delete
        # for published book from the book_read page
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_read_page(book, context)
            page.click(f'text="{self.book_delete_cta}"')
            page.wait_for_selector(f'text="{self.book_delete_confirm_cta}"')
            page.click(f'text="{self.book_delete_confirm_cta}"')

            success_url = self.get_own_book_list_absolute_url()
            self.assertEqual(page.url, success_url)

            # owner sees appropriate success alert on the redirected (destination) page
            alert_elt = page.query_selector(f'text={self.alert_delete_successful}')
            self.assertIsNotNone(alert_elt)
            self.assertIn(book.title, alert_elt.inner_text())
