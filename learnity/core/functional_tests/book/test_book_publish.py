from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookPublishStoryMixin
from core.models import Book


class TestBookPublish(UserStoryMixin, StaticLiveServerTestCase, BookPublishStoryMixin):

    # ctas
    book_publish_cta = _('Publish')
    book_publish_confirm_cta = _('Confirm publish')

    def run_story(self, browser: Browser):
        # run story for unpublished, chapterless book
        chapterless_book = baker.make_recipe('core.tests.book', created_by=self.owner)
        self.run_unpublished_chapterless_book_story(browser, chapterless_book)

        # run story for unpublished book that has chapters
        book = baker.make_recipe('core.tests.book', created_by=self.owner)
        baker.make_recipe('core.tests.chapter', book=book)
        self.run_unpublished_book_story(browser, book)

        # run story for published book
        published_book = baker.make_recipe('core.tests.published_book', created_by=self.owner)
        self.run_published_book_story(browser, published_book)

    def run_unpublished_chapterless_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), False)
        self.assertEqual(book.has_chapters(), False)

        # anonymous user is forbidden from GETing book_read page for
        # unpublished book and therefore cannot initiate book_publish

        # non-owner is forbidden from GETing book_read page for
        # unpublished book and therefore cannot initiate book_publish

        # owner cannot initiate book_publish for unpublished book without
        # chapters because the publish cta isn't present on the book_read page

    def run_unpublished_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), False)
        self.assertEqual(book.has_chapters(), True)

        # anonymous user is forbidden from GETing book_read page for
        # unpublished book and therefore cannot initiate book_publish

        # non-owner is forbidden from GETing book_read page for
        # unpublished book and therefore cannot initiate book_publish

        # owner can initiate and follow through with book_publish
        # for unpublished book with chapters from the book_read page
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_read_page(book, context)
            page.click(f'text="{self.book_publish_cta}"')
            page.wait_for_selector(f'text="{self.book_publish_confirm_cta}"')
            page.click(f'text="{self.book_publish_confirm_cta}"')

            success_url = self.get_book_read_absolute_url(book)
            self.assertEqual(page.url, success_url)

    def run_published_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), True)

        # anonymous user cannot initiate book_publish for published book
        # because the publish cta isn't shown to them on the book_read page

        # non-owner user cannot initiate book_publish for published book
        # because the publish cta isn't shown to them on the book_read page

        # owner cannot initiate book_publish for published book
        # because the publish cta isn't shown to them on the book_read page
