from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookCreateStoryMixin
from core.models import Book


class TestBookCreate(UserStoryMixin, StaticLiveServerTestCase, BookCreateStoryMixin):

    # book_create data
    book_title = 'Rust By Example'
    book_url = 'https://doc.rust-lang.org/rust-by-example/'
    book_abstract = '255 word short description of the book, RBE'
    book_description = 'Long description of RBE. In markdown.'

    # alerts
    alert_create_successful = _("You've just created a book")

    # cta
    book_create_cta = _('Create book')

    def run_story(self, browser: Browser):
        # anonymous user is redirected to login
        with closing(browser.new_context()) as context:
            page = self.get_book_create_page(context)
            expected_redirect_url = self.get_login_absolute_url() + '?next=' + self.get_book_create_path()
            self.assertEqual(page.url, expected_redirect_url)

        # auth user can create book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_create_page(context)

            # page contains book_create form with the correct fields
            title_field = page.query_selector('id=id_title')
            subtitle_field = page.query_selector('id=id_subtitle')
            url_field = page.query_selector('id=id_url')
            abstract_field = page.query_selector('id=id_abstract')
            description_field = page.query_selector('id=id_description')
            self.assertIsNotNone(title_field)
            self.assertIsNotNone(subtitle_field)
            self.assertIsNotNone(url_field)
            self.assertIsNotNone(abstract_field)
            self.assertIsNotNone(description_field)

            # auth user fills out the fields and submits data
            title_field.fill(self.book_title)
            url_field.fill(self.book_url)
            abstract_field.fill(self.book_abstract)
            description_field.fill(self.book_description)
            page.click(f'text="{self.book_create_cta}"')

            # auth user is redirected to book_read page for the created book
            book = Book.objects.filter(created_by_id=self.owner.pk).latest('created_at')
            expected_redirect_url = self.get_book_read_absolute_url(book)
            self.assertEqual(page.url, expected_redirect_url)

            # auth user sees appropriate alert message
            alert_elt = page.query_selector(f'text={self.alert_create_successful}')
            self.assertIsNotNone(alert_elt)
            self.assertIn(book.title, alert_elt.inner_text())
