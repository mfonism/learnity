from contextlib import closing
from typing import List

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookReadStoryMixin
from core.models import Book, Chapter


class TestBookRead(UserStoryMixin, StaticLiveServerTestCase, BookReadStoryMixin):
    def run_story(self, browser: Browser):
        # Alice has a yet unpublished book
        book = baker.make_recipe('core.tests.book', created_by=self.owner)

        # run appropriate story for book in inital state
        self.run_unpublished_chapterless_book_story(book, browser)

        # add chapters to unpublished book and run appropriate story
        num_chapters = 3
        numbers = list(range(num_chapters, 0, -1))
        chapters = baker.make_recipe('core.tests.chapter', number=numbers.pop, book=book, _quantity=num_chapters)
        self.run_unpublished_chaptered_book_story(book, chapters, browser)

        # publish book and run appropriate story
        book.publish()
        self.run_published_book_story(book, chapters, browser)

    def run_unpublished_chapterless_book_story(self, book: Book, browser: Browser):
        self.assertEqual(book.is_published(), False)
        self.assertEqual(book.has_chapters(), False)

        # anonymous user is forbidden from reading unpublished book
        with closing(browser.new_context()) as context:
            page = self.get_book_read_page(book, context)
            self.assert_is_forbidden_page(page)

        # non-owner is forbidden from reading unpublished book
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_read_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can read unpublished book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_read_page(book, context)
            self.assert_page_contains_summary_and_chapter_panes(page, book)
            self.assert_page_summary_pane_contains_book_details(page, book)
            self.assert_delete_button_present_and_working(page, book)
            self.assert_update_button_present_and_working(page, book)
            self.assert_publish_button_not_present(page)
            self.assert_publication_details_not_present(page)
            self.assert_add_chapter_button_present_and_working(page, book)
            self.assert_start_book_journey_button_not_present(page)

    def run_unpublished_chaptered_book_story(self, book: Book, chapters: List[Chapter], browser: Browser):
        self.assertEqual(book.is_published(), False)
        self.assertEqual(book.has_chapters(), True)

        # anonymous user is forbidden from reading unpublished chaptered book
        with closing(browser.new_context()) as context:
            page = self.get_book_read_page(book, context)
            self.assert_is_forbidden_page(page)

        # non-owner is forbidden from reading unpublished chaptered book
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_read_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can read unpublished chaptered book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_read_page(book, context)
            self.assert_publish_button_present_and_working(page, book)
            self.assert_page_chapters_pane_contains_book_chapters(page, book, chapters)
            self.assert_start_book_journey_button_not_present(page)

    def run_published_book_story(self, book: Book, chapters: List[Chapter], browser: Browser):
        self.assertEqual(book.is_published(), True)
        self.assertEqual(book.has_chapters(), True)

        # anonymous user can read published book
        with closing(browser.new_context()) as context:
            page = self.get_book_read_page(book, context)
            self.assert_page_contains_summary_and_chapter_panes(page, book)
            self.assert_page_summary_pane_contains_book_details(page, book)
            self.assert_publication_details_present(page, self.owner.name)
            self.assert_delete_button_not_present(page)
            self.assert_publish_button_not_present(page)
            self.assert_update_button_not_present(page)
            self.assert_page_chapters_pane_contains_book_chapters(page, book, chapters)
            self.assert_add_chapter_button_not_present(page)
            self.assert_start_book_journey_button_present(page, book, is_user_anon=True)

        # non-owner can read published book
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_read_page(book, context)
            self.assert_page_contains_summary_and_chapter_panes(page, book)
            self.assert_page_summary_pane_contains_book_details(page, book)
            self.assert_publication_details_present(page, self.owner.name)
            self.assert_delete_button_not_present(page)
            self.assert_publish_button_not_present(page)
            self.assert_update_button_not_present(page)
            self.assert_page_chapters_pane_contains_book_chapters(page, book, chapters)
            self.assert_add_chapter_button_not_present(page)
            self.assert_start_book_journey_button_present(page, book, is_user_anon=False)

        # owner can read published book
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_read_page(book, context)
            self.assert_page_contains_summary_and_chapter_panes(page, book)
            self.assert_page_summary_pane_contains_book_details(page, book)
            self.assert_publication_details_present(page, _('You'))
            self.assert_publish_button_not_present(page)
            self.assert_start_book_journey_button_present(page, book, is_user_anon=False)
