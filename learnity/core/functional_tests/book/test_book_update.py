from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookUpdateStoryMixin
from core.models import Book


class TestBookUpdate(UserStoryMixin, StaticLiveServerTestCase, BookUpdateStoryMixin):

    # book update data
    updated_title = 'Updated Title'
    updated_subtitle = 'Updated Subtitle'
    updated_url = 'https://www.updated.url/'

    # alerts
    alert_update_successful = _("You've just updated the book")

    # cta
    book_update_cta = _('Update book')

    def run_story(self, browser: Browser):
        # run story for unpublished book
        book = baker.make_recipe('core.tests.book', created_by=self.owner)
        self.run_unpublished_book_story(browser, book)

        # run story for published book
        published_book = baker.make_recipe('core.tests.published_book', created_by=self.owner)
        self.run_published_book_story(browser, published_book)

    def run_unpublished_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), False)

        path = self.get_book_update_path(book)
        redirect_url = self.get_login_absolute_url() + '?next=' + path

        # anonymous user is redirected to login url
        # on attempting to GET book_update page
        with closing(browser.new_context()) as context:
            page = self.get_book_update_page(book, context)
            self.assertEqual(page.url, redirect_url)

        # non-owner is forbidden from GET'ing book_update page
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_update_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can GET book_update page and POST data with it
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_update_page(book, context)

            # page contains book_update form with the correct fields
            title_field = page.query_selector('id=id_title')
            subtitle_field = page.query_selector('id=id_subtitle')
            abstract_field = page.query_selector('id=id_abstract')
            url_field = page.query_selector('id=id_url')
            description_field = page.query_selector('id=id_description')
            self.assertIsNotNone(title_field)
            self.assertIsNotNone(subtitle_field)
            self.assertIsNotNone(abstract_field)
            self.assertIsNotNone(url_field)
            self.assertIsNotNone(description_field)

            # owner fills out some fields and submits data
            title_field.fill(self.updated_title)
            subtitle_field.fill(self.updated_subtitle)
            url_field.fill(self.updated_url)
            page.click(f'text="{self.book_update_cta}"')
            book.refresh_from_db()

            # owner is redirected to book_read page
            # with an appropriate alert message
            book_read_url = self.get_book_read_absolute_url(book)
            self.assertEqual(page.url, book_read_url)

            success_alert_elt = page.query_selector(f'text={self.alert_update_successful}')
            self.assertIsNotNone(success_alert_elt)
            self.assertIn(book.title, success_alert_elt.inner_text())

    def run_published_book_story(self, browser: Browser, book: Book):
        self.assertEqual(book.is_published(), True)

        path = self.get_book_update_path(book)
        redirect_url = self.get_login_absolute_url() + '?next=' + path

        # anonymous user is redirected to login url
        # on attempting to GET book_update page
        with closing(browser.new_context()) as context:
            page = self.get_book_update_page(book, context)
            self.assertEqual(page.url, redirect_url)

        # non-owner is forbidden from GET'ing book_update page
        with closing(browser.new_context()) as context:
            self.login_non_owner(context)
            page = self.get_book_update_page(book, context)
            self.assert_is_forbidden_page(page)

        # owner can GET book_update page and POST data with it
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_update_page(book, context)

            # page contains book_update form with the correct fields
            title_field = page.query_selector('id=id_title')
            subtitle_field = page.query_selector('id=id_subtitle')
            abstract_field = page.query_selector('id=id_abstract')
            url_field = page.query_selector('id=id_url')
            description_field = page.query_selector('id=id_description')
            self.assertIsNotNone(title_field)
            self.assertIsNotNone(subtitle_field)
            self.assertIsNotNone(abstract_field)
            self.assertIsNotNone(url_field)
            self.assertIsNotNone(description_field)

            # title field and subtitle field are disabled
            self.assertEqual(title_field.is_disabled(), True)
            self.assertEqual(subtitle_field.is_disabled(), True)

            # owner fills out some fields and submits data
            url_field.fill(self.updated_url)
            page.click(f'text="{self.book_update_cta}"')
            book.refresh_from_db()

            # owner is redirected to book_read page
            # with an appropriate alert message
            book_read_url = self.get_book_read_absolute_url(book)
            self.assertEqual(page.url, book_read_url)

            success_alert_elt = page.query_selector(f'text={self.alert_update_successful}')
            self.assertIsNotNone(success_alert_elt)
            self.assertIn(book.title, success_alert_elt.inner_text())
