from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookListStoryMixin


class TestOwnBookList(UserStoryMixin, StaticLiveServerTestCase, BookListStoryMixin):

    add_new_book_cta = _('Add new book')

    def run_story(self, browser: Browser):
        # Alice has 4 books
        # 1 published, 3 unpublished
        num_own_unpublished_books = 3
        num_own_published_books = 1
        own_unpublished_books = baker.make_recipe(
            'core.tests.book', created_by=self.owner, _quantity=num_own_unpublished_books
        )
        own_published_books = baker.make_recipe(
            'core.tests.published_book', created_by=self.owner, _quantity=num_own_published_books
        )

        # there are five other books
        # 3 published, 2 unpublished
        num_other_unpublished_books = 2
        num_other_published_books = 3
        other_unpublished_books = baker.make_recipe('core.tests.book', _quantity=num_other_unpublished_books)
        other_published_books = baker.make_recipe('core.tests.published_book', _quantity=num_other_published_books)

        # anonymous user is redirected to login
        with closing(browser.new_context()) as context:
            page = self.get_own_book_list_page(context)
            expected_redirect_url = self.get_login_absolute_url() + '?next=' + self.get_own_book_list_path()
            self.assertEqual(page.url, expected_redirect_url)

        # owner can see own book list
        # with all their books present, whether published or not
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_own_book_list_page(context)
            self.assert_page_contains_books(page, own_published_books)
            self.assert_page_contains_books(page, own_unpublished_books)
            self.assert_page_does_not_contain_books(page, other_published_books)
            self.assert_page_does_not_contain_books(page, other_unpublished_books)

            # add new book button is present
            page.click(f'text="{self.add_new_book_cta}"')
            self.assertEqual(page.url, self.get_book_create_absolute_url())
