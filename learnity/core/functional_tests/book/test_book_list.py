from contextlib import closing

from playwright.sync_api._generated import Browser

from django.utils.translation import gettext as _

from model_bakery import baker

from core.functional_tests.mixins import StaticLiveServerTestCase, UserStoryMixin
from core.functional_tests.mixins.book import BookListStoryMixin


class TestBookList(UserStoryMixin, StaticLiveServerTestCase, BookListStoryMixin):

    add_new_book_cta = _('Add new book')

    def run_story(self, browser: Browser):
        # a number of unpublished books
        num_unpublished_books = 2
        unpublished_books = baker.make_recipe('core.tests.book', _quantity=num_unpublished_books)
        # a number of published books
        num_published_books = 3
        published_books = baker.make_recipe('core.tests.published_book', _quantity=num_published_books)

        # anonymous user can see book list
        # only published books are present
        with closing(browser.new_context()) as context:
            page = self.get_book_list_page(context)
            self.assert_page_contains_books(page, published_books)
            self.assert_page_does_not_contain_books(page, unpublished_books)

            # add new book button is present
            page.click(f'text="{self.add_new_book_cta}"')
            expected_redirect_url = self.get_login_absolute_url() + '?next=' + self.get_book_create_path()
            self.assertEqual(page.url, expected_redirect_url)

        # auth user can see book list
        # only published books are present
        with closing(browser.new_context()) as context:
            self.login_owner(context)
            page = self.get_book_list_page(context)
            self.assert_page_contains_books(page, published_books)
            self.assert_page_does_not_contain_books(page, unpublished_books)

            # add new book button is present
            page.click(f'text="{self.add_new_book_cta}"')
            self.assertEqual(page.url, self.get_book_create_absolute_url())
