import sys


def is_running_functional_tests():
    if 'test' in sys.argv:
        for arg in sys.argv:
            if 'functional_tests' in arg:
                return True
    return False
